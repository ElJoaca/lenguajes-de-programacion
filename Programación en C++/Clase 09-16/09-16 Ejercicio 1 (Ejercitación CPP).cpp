/* 16 de septiembre ejercicio 1
1) Desarrollar un programa en lenguaje C++ con una clase para operar con n�meros
naturales respetando sus propiedades:
	- La clase permitir� crear objetos que almacenen s�lo n�meros naturales
	(positivos y cero)
	- Se deber�n poder realizar las operaciones suma, resta, multiplicaci�n y
	divisi�n cuyo resultado ser� siempre un numero natural
Se pide:
a. Definici�n de la clase Natural (interface):
	- Atributo privado entero
	- M�todos detallados a continuaci�n
b. Implementaci�n de los m�todos siguientes:
	i. Constructor ordinario
	ii. Constructor por defecto
	iii. Constructor de copia
	iv. Sobrecarga de los operadores +, -, * y /
	v. M�todo set que s�lo asigne un valor al atributo privado si es un numero
	natural, cero en caso contrario
	vi. M�todo get del atributo privado
b. Implementar un programa main para demostrar el funcionamiento correcto de la
clase
*/

#include <iostream>
using namespace std;

class Natural {
	int n;
	
	public:
	Natural ();																	//Funci�n constructor por defecto.
	Natural (int);																//Funci�n constructor ordinario.
	Natural (Natural const &);													//Funci�n constructor de copia.
	Natural operator* (Natural);												//Sobre carga de operador como m�todo de la clase.
	friend Natural operator/ (Natural, Natural);								//Sobrecarga del operator como funci�n amiga.
	void setn (int);
	int getn ();
	void imprimir ();
};

Natural operator+  (Natural, Natural);											//Sobrecarga de operador como funci�n fuera de la clase.
Natural operator- (Natural, Natural);

Natural::Natural ()
{
	n= 1;
}

Natural::Natural (int num)
{
	if (num >= 0)
	{
		n= num;
	}
	else
	{
		n=0;
	}
}

Natural::Natural (Natural const &num)	
{
	if (num.n >= 0)
	{
		n= num.n;
	}
	else
	{
		n=0;
	}
}

Natural operator+  (Natural num1, Natural num2)									//Sobrecarga de operador como funci�n fuera de la clase.
{
	/* 1�) Alternativa: */														//"Regreso un objeto de la clase Natural"
	return (num1.getn() + num2.getn());											//Llama al constructor ordinario.
	/* 2�) Alternativa: */
	//	Natural num3;															//Llama al constructor por defecto.
	//	num3.setn(num1.getn() + num2.getn());
	//	return num3;
	/* 3�) Alternativa */														//Llama al constructor ordinario. NO FUNCIONA porque setn no devuelve nada.
	//Natural num3= Natural(num3.setn(num1.getn() + num2.getn()));
	//return num3;
	/* 4�) Alternativa */														//Llama al constructor ordinario. Una forma an�loga a la Alternativa 3�). NO FUNCIONA porque setn no devuelve nada.
	//Natural num3(num3.setn(num1.getn() + num2.getn()));
	//return num3;
}

Natural operator- (Natural num1, Natural num2)
{
	Natural num3= Natural ();
	if (num1.getn () > num2.getn ())
	{
		num3.setn (num1.getn () - num2.getn ());
	}
	else
	{
		num3.setn (0);
	}
	
	return num3;
}

Natural Natural::operator*  (Natural num1)										//Sobrecarga de operador como m�todo de la clase.
{
	/* 1�) Alternativa: */
	//Natural num2= Natural (this -> n * num1.n);
	//return  num2;
	/* 2�) Alternativa: */
	return (this -> n * num1.n);

}

Natural operator/ (Natural num1, Natural num2)
{
	return (num1.n / num2.n);
}


void Natural::setn (int num)
{
	if (num >= 0)
	{
		n= num;
	}
	else
	{
		cout << "Error: El valor debe ser natural" << endl;
	}
}

int Natural::getn (void)
{
	return n;
}

void Natural::imprimir ()
{
	cout << "\n- El numero obtenido es: " << n << "." << endl;
}

int main(int argc, char *argv[]) {
	cout << "Funcion constructor:";
	Natural n1;
	n1.imprimir ();
	
	cout << "\nFuncion constructor ordinario:";
	Natural n2 (2);
	n2.imprimir ();
	
	cout << "\nFuncion constructor de copia (copia un objeo en otro)";
	Natural n3= n2;																//Se llama cuando creo una nueva variable de la clase.
	n3.imprimir ();
	
	cout << "\nOperator+:";
	Natural n4;
	n4= n2 + n2;
	n4.imprimir ();
	
	cout << "\nOperator-:";
	Natural n5;
	n5= n2 - n1;
	n5.imprimir ();
	
	cout << "\nOperator*:";
	Natural n6;
	n6= n2.operator*(n4) ;
	n6.imprimir ();
	
	cout << "\nOperator/:";
	Natural n7;
	n7= n4 / n2;
	n7.imprimir ();
	
	return 0;
}
