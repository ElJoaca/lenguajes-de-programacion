/* 16 de septiembre ejercicio 2
Desarrollar un programa en lenguaje C++ con una clase Rect�ngulo con las 
siguientes caracter�sticas:
a. Atributos privados enteros base y altura
b. M�todos:
	i. Constructor ordinario y por defecto
	ii. M�todos set y get de sus atributos (recordar que sus atributos no
	podr�n ser negativos)
	iii. M�todo imprimir que muestre en pantalla un rect�ngulo formado por
	un car�cter recibido como argumento
	iv. M�todos para obtener el �rea y el per�metro del rect�ngulo
	v. M�todo para determinar si el rect�ngulo es un cuadrado*/

#include <iostream>
using namespace std;

class rectangulo {
	int base;
	int altura;
	
	public:
	rectangulo ();
	rectangulo (int, int);
	void area (int, int);
	void perimetro (int, int);
	void cuadrado (int, int);
	void dibujo (int, int);
	void set (int, int);
	int get_base ();
	int get_altura ();
	void imprimir ();
};

int main(int argc, char *argv[]) {
	cout << "Funcion constructor por defecto:" << endl;
	rectangulo r1;
	r1.imprimir ();
	r1.area (r1.get_base(), r1.get_altura ());
	r1.perimetro (r1.get_base (), r1.get_altura ());
	r1.cuadrado (r1.get_base (), r1.get_altura ());
	r1.dibujo (r1.get_base (), r1.get_altura ());
	
	cout << "\nFuncion constructor ordinario:" << endl;
	rectangulo r2 (2, 4);
	r2.imprimir ();
	r2.area (r2.get_base (), r2.get_altura ());
	r2.perimetro (r2.get_base (), r2.get_altura ());
	r2.cuadrado (r2.get_base (), r2.get_altura ());
	r2.dibujo (r2.get_base (), r2.get_altura ());
	
	cout << "\nMetodo set:" << endl;
	rectangulo r3;
	r3.set(3, 6);
	r3.imprimir ();
	r3.area (r3.get_base(), r3.get_altura ());
	r3.perimetro (r3.get_base (), r3.get_altura ());
	r3.cuadrado (r3.get_base (), r3.get_altura ());
	r3.dibujo (r3.get_base (), r3.get_altura ());
	
	cout << "\nMetodo get:" << endl;
	rectangulo r4;
	r4.set(r3.get_base (), r3.get_altura ());
	r4.imprimir ();
	r4.area (r4.get_base(), r4.get_altura ());
	r4.perimetro (r4.get_base (), r4.get_altura ());
	r4.cuadrado (r4.get_base (), r4.get_altura ());
	r4.dibujo (r4.get_base (), r4.get_altura ());
	
	return 0;
}

rectangulo::rectangulo ()
{
	base= 1;
	altura= 2;
}

rectangulo::rectangulo (int num1, int num2)
{
	base= num1;
	altura= num2;
}

void rectangulo::area (int num1, int num2)
{
	cout << "Area: " << num1 * num2 << endl;
}

void rectangulo::perimetro (int num1, int num2)
{
	cout << "Perimetro: " << num1*2 + num2*2 << endl;
}

void rectangulo::cuadrado (int num1, int num2)
{
	if (num1 == num2)
	{
		cout << "Cuadrado" << endl;
	}
	else
	{
		cout << "Figura: Rectangulo" << endl;
	}
}

int rectangulo::get_base ()
{
	return base;
}

void rectangulo::dibujo (int base, int altura)
{
	int i= 0;
	int j=0;
	
	for (i=0; i<altura; i++)
	{
		for (j=0; j<base; j++)
		{
			cout << "*";
		}
		cout << endl;
	}
}

void rectangulo::set (int num1,int num2)
{
	base= num1;
	altura= num2;
}

int rectangulo::get_altura ()
{
	return altura;
}

void rectangulo::imprimir ()
{
	cout << "Valor de la base: " << base << endl;
	cout << "Valor de la altura: " << altura << endl;
}
