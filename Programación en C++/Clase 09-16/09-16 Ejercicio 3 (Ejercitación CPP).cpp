/* 16 de septiembre ejercicio 3
Desarrollar un programa en lenguaje C++ que implemente 3 funciones para elevar 
al cuadrado un numero entero (se puede aplicar sobrecarga de funciones):
a. Pasando el par�metro por valor y devolviendo el resultado
b. Pasando el par�metro por referencia mediante puntero, guardando el resultado 
en la posici�n de memoria recibida
c. �dem anterior mediante referencia o alias
Desde el programa main, verificar el funcionamiento de las 3 funciones mostrando 
en pantalla desde main el valor original y su cuadrado para cada una.*/

#include <iostream>
using namespace std;

int cuadrado_por_valor (int);
void cuadrado_por_referencia (int *);
void cuadrado_por_alias (int &);

int main(int argc, char *argv[]) {
	int n;
	
	cout << "!!!Hello World!!!" << endl;
	cout << "Ingrese valor: ";
	cin >> n;
	cout << "El cuadrado (por valor) de " << n << " es: ";
	n= cuadrado_por_valor(n);
	cout << n << endl;
	cout << "Ingrese valor: ";
	cin >> n;
	cout << "El cuadrado (por puntero) de " << n << " es: ";
	cuadrado_por_referencia(&n);
	cout << n << endl;
	cout << "Ingrese valor: ";
	cin >> n;
	cout << "El cuadrado (por referencia / alias) de " << n << " es: ";
	cuadrado_por_alias(n);
	cout << n << endl;
	return 0;
}

int cuadrado_por_valor (int n)													//Se copian los valores.
{ 
	return n*n;
}

void cuadrado_por_referencia (int *n)
{
	*n= (*n) * (*n);
}

void cuadrado_por_alias (int &n)												//"Le pasa la direcci�n y trabajas como si fuera un puntero". Es lo mismo que paso por puntero pero "m�s prolijo". El & crea un alias / distintos nombres para la misma variable. 
{
	n=  n * n;
}
