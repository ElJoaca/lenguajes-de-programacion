/* 1 de abril ejercicio 1*/

#include <stdio.h>
#include <math.h>

int main () {

  printf ("Raiz cuadrada de %lf es %lf\n", 16.0, sqrt(16.0));
  printf ("Raiz cuadrada de %lf es %lf\n", 5.0, sqrt(5.0));

  return(0);
}
