/* 1 de abril ejercicio 2
Escribir un programa en C que lea un car�cter y lo imprima en pantalla en 
may�scula.
Dato de entrada: letra en min�scula (verificar)*/

#include <stdio.h>
#include <math.h>

int main () {
  char c;

  printf ("Ingrese una letra: ");
  scanf ("%c", &c);
  if (c >= 97 && c <= 122)
  {
    c=c-32;
    printf ("Letra ingresada: %c\n", c);
  }
  else
  {
    printf ("Error: Debe ingresar una letra en minuscula\n");
  }

  return(0);
}
