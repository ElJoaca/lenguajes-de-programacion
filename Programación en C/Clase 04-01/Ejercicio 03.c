/* 1 de abril ejercicio 3
Escribir un programa en C que lea los valores de los catetos de un tri�ngulo y 
calcule cual es la hipotenusa, el �rea y el per�metro.*/

#include <stdio.h>
#include <math.h>

int main () {

	float c1;
	float c2;
	float h;
	float a;
	float p;

	printf ("Ingrese el largo del cateto 1: ");
	scanf ("%f", &c1);
	printf ("Ingrese el largo del cateto 2: ");
	scanf ("%f", &c2);
	h= sqrt(c1*c1+c2*c2);
	printf ("Hipotenusa= %f\n", h);
	a=(c1*c2)/2;
	printf ("Area= %f\n", a);
	p= c1+c2+h;
	printf ("Perimetro= %f\n", p);

	return(0);
}
