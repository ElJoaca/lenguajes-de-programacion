/* 8 de abril ejercicio 2
Escribir un programa en C que lea un caracter y lo imprima en pantalla en
may�scula.*/

#include <stdio.h>
#include <math.h>

int main () {
	char c= 0;

	printf ("Ingrese una letra: ");
	scanf ("%c", &c);
	while (c < 97 || c > 122)
	{
		printf ("\nError: Debe ingresar una letra en minuscula: ");
		scanf ("%c", &c);
	}
	 c= c-32;
	 printf ("\nLetra ingresada convertida a mayuscula: %c", c);


  return(0);
}
