/* 8 de abril ejercicio 4
Escribir un programa en C que mediante las funciones scanf y printf genere el
mensaje "Por favor, introduce tu nombre:" y que el usuario pueda ingresar su
nombre en la misma linea. Asignar el nombre en un arreglo de caracteres llamado
nombre.*/

#include <stdio.h>

int main(int argc, char *argv[]) {
	char nombre [10];
	
	printf("Por favor, introduce tu nombre: ");
	scanf ("%s", &nombre);
	printf("\nEl nombre ingresado es: %s.", nombre);
	
	return 0;
}

