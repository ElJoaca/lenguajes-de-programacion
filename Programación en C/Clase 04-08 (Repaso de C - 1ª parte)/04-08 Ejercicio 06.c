/* 8 de abril ejercicio 6
Leer una l�nea de texto que contenga letras may�sculas y min�sculas. Escribir el
texto con letras may�sculas y min�sculas intercambiadas y el resto de los
caracteres intactos. (Sugerencia: Utilizar el operador condicional ? : y las
funciones de biblioteca islower, tolower ytoupper).*/

#include <stdio.h>

int main(int argc, char *argv[]) {
	int i;
	char string [10];
	
	printf ("Ingrese un nick de hasta 10 caracteres: ");
	scanf ("%s", &string);
	for (i= 0; i < 10; i++)
	{
		if ( string [i] >= 65 && string [i] <= 90)
		{
			string [i]= string [i] + 32;
		}
		else
		{
			if ( string [i] >= 97 && string [i] <= 122)
			{
				string [i]= string [i] - 32;
			}
		}
	}
	printf ("Nick con mayusculas y minusculas intercambiadas: %s", string);
	return 0;
}

