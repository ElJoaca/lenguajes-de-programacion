/* 8 de abril ejercicio 7
Solicitar al usuario que introduzca 10 n�meros enteros. Ordenarlos de menor a
mayor y mostrarlos en pantalla.*/

#include <stdio.h>

int main(int argc, char *argv[]) {
	int vector [10];
	int aux;
	int i, j;
	
	for (i=0; i<10; i++)
	{
		printf ("Indruduzca un numero: %d/10: ", i+1);
		scanf ("%d", &vector[i]);
	}
	for (i=0; i<10-1; i++)
	{
		for (j=0; j<10-1-i; j++)
		{
			if ( vector[j] > vector[j+1])
			{
				aux= vector[j];
				vector[j]= vector[j+1];
				vector[j+1]= aux;
			}
		}
	}
	printf ("\nNumeros ordenados de menor a mayor:\n");
	for (i=0; i<10; i++)
	{
		printf ("\n%d", vector[i]);
	}
	return 0;
}

