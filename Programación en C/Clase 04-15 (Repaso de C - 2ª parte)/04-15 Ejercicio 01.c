/* 15 de abril ejercicio 1
Un numero primo es una cantidad entera que es divisible (sin resto) solo por 1 y 
por si mismo. Calcular y presentar una lista con los n primeros n�meros primos. 
Recordar que un numero ser� primo si los restos de las ecuaciones n/2, n/3, n/4,
..., son no nulos. Comprobar el programa haciendo que calcule los 100 primeros 
n�meros primos.*/

#include <stdio.h>

int main(int argc, char *argv[]) {
	int numeros_a_mostrar;
	int numero_prueba= 2;
	int esprimo= 0;
	int i=0, j;
		
	printf ("Ingrese el valor de n: ");
	scanf ("%d", &numeros_a_mostrar);
	
	while (i < numeros_a_mostrar)
	{
		esprimo= 1;
		for (j= 2; j< numero_prueba-1; j++)
		{
			if (numero_prueba%j == 0)
			{
				esprimo= 0;
				break;
			}
		}
		if (esprimo == 1)
		{
			printf ("Nro primo %d: %d \n", i+1, numero_prueba);
			i++;
		}
		numero_prueba++;
	}
	return 0;
}

