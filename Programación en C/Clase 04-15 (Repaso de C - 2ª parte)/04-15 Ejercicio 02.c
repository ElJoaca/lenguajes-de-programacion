/* 15 de abril ejercicio 2
Escribir un programa en C que permita que se pueda utilizar la computadora como 
una calculadora normal. Considerar solo las operaciones aritm�ticas b�sicas 
(suma, resta, multiplicaci�n y divisi�n). Incluir una memoria en la que se
pueda almacenar un numero.*/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

int main(int argc, char *argv[]) {
	float op1;
	float op2;
	float result;
	float mem=0;
	char menu;
	char opcion;
	
	do
	{
		system ("cls");
		printf ("Calculadora:");
		printf ("\n\n1) Suma.");
		printf ("\n2) Resta.");
		printf ("\n3) Multiplicaci%cn.", 162);
		printf ("\n4) Divisi%cn.", 162);
		printf ("\n5) Ver memoria.");
		printf ("\nesc) para salir.");
		printf ("\n\nIngresar opci%cn: ", 162);
		menu= getch ();
		switch (menu)
		{
			case '1':
				system ("cls");
				printf ("Suma:");
				printf ("\n\nIngrese el primer sumando: ");
				scanf ("%f", &op1);
				printf ("Ingrese el segundo sumando: ");
				scanf ("%f", &op2);
				result= op1 + op2;
				printf ("\nLa suma es: %.2f\n", result);
				do
				{
					printf ("\n�Desea guardar en memoria? s/n: ");
					scanf (" %c", &opcion);
					switch (opcion)
					{
						case 's':
							mem= mem + result;
							printf ("\nValor guardado con %cxito\n\n", 130);
							system ("pause");
							break;
						case 'n':
							break;
						default:
							printf ("\nError fatal:");
							printf ("\n\n%cLa opci%cn ingresada no es v%clida.\n\n", 07, 162, 160);
							system ("pause");
							break;
					}
				} while(opcion != 's' && opcion != 'n');
				break;
			case '2':
				system ("cls");
				printf ("Resta:");
				printf ("\n\nIngrese el minuendo: ");
				scanf ("%f", &op1);
				printf ("Ingrese el sustraendo: ");
				scanf ("%f", &op2);
				result= op1 - op2;
				printf ("\nLa diferencia es: %.2f\n", result);
				do
				{
					printf ("\n�Desea guardar en memoria? s/n: ");
					scanf (" %c", &opcion);
					switch (opcion)
					{
					case 's':
						mem= mem - result;
						printf ("\nValor guardado con %cxito\n\n", 130);
						system ("pause");
						break;
					case 'n':
						break;
					default:
						printf ("\nError fatal:");
						printf ("\n\n%cLa opci%cn ingresada no es v%clida.\n\n", 07, 162, 160);
						system ("pause");
						break;
					}
				} while(opcion != 's' && opcion != 'n');
				break;
			case '3':
				system ("cls");
				printf ("Multiplicaci%cn:", 162);
				printf ("\n\nIngrese el primer factor: ");
				scanf ("%f", &op1);
				printf ("Ingrese el segundo factor: ");
				scanf ("%f", &op2);
				result= op1 * op2;
				printf ("\nEl producto es: %.2f\n", result);
				do
				{
					printf ("\n�Desea guardar en memoria? s/n: ");
					scanf (" %c", &opcion);
					switch (opcion)
					{
					case 's':
						mem= mem * result;
						printf ("\nValor guardado con %cxito\n\n", 130);
						system ("pause");
						break;
					case 'n':
						break;
					default:
						printf ("\nError fatal:");
						printf ("\n\n%cLa opci%cn ingresada no es v%clida.\n\n", 07, 162, 160);
						system ("pause");
						break;
					}
				} while(opcion != 's' && opcion != 'n');
				break;
			case '4':
				system ("cls");
				printf ("Divisi%cn:", 162);
				printf ("\n\nIngrese el dividendo: ");
				scanf ("%f", &op1);
				printf ("Ingrese el divisor: ");
				scanf ("%f", &op2);
				result= op1 / op2;
				printf ("\nEl cociente es: %.2f\n", result);
				do
				{
					printf ("\n�Desea guardar en memoria? s/n: ");
					scanf (" %c", &opcion);
					switch (opcion)
					{
					case 's':
						mem= mem / result;
						printf ("\nValor guardado con %cxito\n\n", 130);
						system ("pause");
						break;
					case 'n':
						break;
					default:
						printf ("\nError fatal:");
						printf ("\n\n%cLa opci%cn ingresada no es v%clida.\n\n", 07, 162, 160);
						system ("pause");
						break;
					}
				} while(opcion != 's' && opcion != 'n');
				break;
			case '5':
				system ("cls");
				printf ("Memoria:");
				printf ("\n\nValor almacenado en memoria: %.2f\n\n", mem);
				system ("pause");
				break;
			case 27:
				system ("cls");
				printf ("Salir:");
				printf ("\n\nPrograma finalizado correctamente.\n\n");
				system ("pause");
				break;
			default:
				system ("cls");
				printf ("ERROR fatal:");
				printf ("\n\n%cLa opci%cn ingresada no es v%clida.\n\n", 07, 162, 160);
				system ("pause");
		}
	} while (menu != 27);
	
	return 0;
}
