/* 22 de abril ejercicio 1
Escribe una funcion en C que, al recibir como dato una matriz de numeros enteros 
(de tamano N recibido como argumento), calcule su traspuesta. La traspuesta de 
una matriz se obtiene al escribir las filas de la matriz como columnas. Dato: 
MAT[M][N] (arreglo bidimensional de tipo entero de M filas y N columnas, 1 . M . 
50 y 1 . N . 50)*/

#include <stdio.h>

void traspuesta (int[][50], int, int);

int main(int argc, char *argv[]) {
	
	int matriz[50][50];
	int M, N;
	int i, j;
	
	printf("Ingrese el numero de filas: ");
	scanf("%d", &M);
	printf("Ingrese el numero de columnas: ");
	scanf("%d", &N);
	printf("\n");
	for (i=0; i<M; i++)
	{
		for (j=0; j<N; j++)
		{
			printf("Ingrese el elemento %d:%d: ", i, j);
			scanf("%d", &matriz[i][j]);
		}
	}
	printf("\nMatriz original:\n\n");
	for (i=0; i<M; i++)
	{
		for (j=0; j<N; j++)
		{
			printf("%d ", matriz[i][j]);
		}
		printf("\n");
	}
	printf("\nMatriz traspuesta:\n\n");
	traspuesta(matriz, M, N);
	for (i=0; i<N; i++)
	{
		for (j=0; j<M; j++)
		{
			printf("%d ", matriz[i][j]);
		}
		printf("\n");
	}
	return 0;
}

void traspuesta (int matriz[][50], int M, int N)
{
	int Tam1, Tam2;
	int aux;
	int i, j;
	
	if (M > N)
	{
		Tam1= N;
		Tam2= M;
	}
	else
	{
		Tam1= M;
		Tam2= N;
	}
	
	for(i=0; i<Tam1; i++)
	{
		for(j=0; j<Tam2; j++)
		{
			if (i < j)
			{
				aux= matriz[j][i];
				matriz[j][i]= matriz[i][j];
				matriz[i][j]= aux;
			}
		}
	}
}
