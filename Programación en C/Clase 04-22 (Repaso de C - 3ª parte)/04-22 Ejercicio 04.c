/* 22 de abril ejercicio 4
Implementar una funcion en C para el calculo del Maximo Com�n Divisor de dos 
n�meros enteros positivos x e y. Si y es igual a 0, entonces MCD(x,y) es x, de 
lo contrario MCD(x,y) es MCD(y,x%y), en donde % es el operador modulo.*/

#include <stdio.h>

int MCD (int a, int b)
{
	if (a == 0)
	{
		return b;
	}
	return MCD (b % a, a);
}

int main(int argc, char *argv[]) {
	int num1, num2;
	
	printf ("Ingrese un n�mero entero: ");
	scanf ("%d", &num1);
	printf ("Ingrese un n�mero entero: ");
	scanf ("%d", &num2);
	printf ("El m�ximo comun divisor es: %d", MCD(num1, num2));
	return 0;
}

