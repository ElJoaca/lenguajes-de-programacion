/* 6 de mayo ejercicio 3
Escribir un programa en el que se defina un arreglo de 10 punteros a float, 
se lean diez n�meros en las ubicaciones en las que hacen referencia los 
punteros. Se sumen todos los n�meros y se almacene el resultado en una direcci�n 
a la que haga referencia un puntero. El programa deber�a mostrar el contenido de 
todas las variables, tanto los punteros como los n�meros de tipo float.*/

#include <stdio.h>

int main(int argc, char *argv[]) {
	
	int i;
	float valores[10]={2,4,6,8,10,1,3,5,7,9};
	float *arreglo [10]; 														//Ocupa en memoria 40 bytes
	float *suma;
	
	for (i=0; i<10; i++)														//inicializamos el vector de arreglo
	{
		arreglo[i]=&valores[i];
	}
	for (i=0; i<10; i++)
	{
		*suma += *arreglo[i];													//*suma += *(arreglo+i);
	}
	printf("Contenido del vector:\n\n");
	for (i=0; i<10; i++)
	{
		printf("Elemento %d: %p, %.2f\n", i, arreglo[i], *arreglo[i]); 			//*(arreglo+i)
	}
	printf ("\nSuma:\n\n");
	printf("%p, %.2f", suma, *suma);
	return 0;
}

