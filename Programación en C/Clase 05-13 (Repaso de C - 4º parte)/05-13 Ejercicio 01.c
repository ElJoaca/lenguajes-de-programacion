/* 13 de mayo ejercicio 1
Mediante un programa en C verifique los tama�os que ocupan en tu PC variables de 
los siguientes tipos de datos:*/

#include <stdio.h>

int main(int argc, char *argv[]) {
	char a;
	unsigned char b;
	short int c;
	unsigned short int d;
	int e;
	unsigned int f;
	long int g;
	unsigned long int h;
	float i;
	double j;
	long double k;
	float *l;
	
	printf("Tama�o char: %d\n", sizeof(a));
	printf("Tama�o unsigned char: %d\n", sizeof(b));
	printf("Tama�o short int: %d\n", sizeof(c));
	printf("Tama�o unsigned short int: %d\n", sizeof(d));
	printf("Tama�o int: %d\n", sizeof(e));
	printf("Tama�o unsigned int: %d\n", sizeof(f));
	printf("Tama�o long int: %d\n", sizeof(g));
	printf("Tama�o unsigned long int: %d\n", sizeof(h));
	printf("Tama�o float: %d\n", sizeof(i));
	printf("Tama�o double: %d\n", sizeof(j));
	printf("Tama�o long double: %d\n", sizeof(k));
	printf("Tama�o *puntero cualquier tipo: %d\n", sizeof(l));
	
	return 0;
}
