/* 13 de mayo ejercicio 4
Escribir una funci�n que reciba como argumento un arreglo de caracteres y lo 
devuelva invertido con la interfaz:
void string_reverse(char *);*/


#include <stdio.h>

void reverse (char *);

int main(int argc, char *argv[]) {
	char cadena []= "12345678";
	
	printf("%s\n", cadena);
	reverse(cadena);
	printf("\n\n%s", cadena);
	
	return 0;
}

void reverse (char *cadena)
{
	int n;
	int i;
	char temp;
	
	printf("\n");
	//n= sizeof(cadena);														//Alternativa: n= printf("%s", cadena); Porque printf devuelve la cantidad de bytes que pudo leer o escribir. Tanto el sizeof como el printf me devuelven el tama�o de la cadena sin incluir el \0 -> Se agregaba un \0 en la �ltima posici�n de memoria que era un caracter que no se imprimia.
	n= printf("%s", cadena);
	printf("\n\nTama%co de la cadena: %d\n\n", 164, n);
	for (i=0; i<n/2; i++)														//Suponiendo n par
	{
		temp= cadena[i];														//Guarda el primer char
		printf("%c, ", temp);
		cadena[i]= cadena[n-1-i];												//En la primer posicion copio el �ltimo char
		printf("%c; ", cadena[i]);
		cadena[n-1-i]= temp;													//guardo al ultimo el primer valor
	}
}
