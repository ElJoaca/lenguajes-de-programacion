/* 13 de mayo ejercicio 5
Escribir una funci�n que reciba una cadena de caracteres s y un arreglo de 
caracteres con espacio suficiente t, y copie la cadena en el arreglo, terminando 
la cadena con el caracter �\0� (funcion strcpy() de la biblioteca ):
void strcpy(char *t, const char *s);*/

#include <stdio.h>

void my_strcpy(char *destination, const char *source)
{	
	while (*source != '\0')
	{
		*destination= *source;
		destination++;
		source++;
	}
	*destination= '\0';
}

int main(int argc, char *argv[]) {
	char cadena1 [10]= "123456789";
	char cadena2 [10];
	printf("Cadena 1: %s", cadena1);
	my_strcpy(cadena2, cadena1);
	printf("\n\nCadena 2: %s", cadena2);
	
	return 0;
}
