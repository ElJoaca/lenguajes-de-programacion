/* 13 de mayo ejercicio 6
a. Escribir una funci�n que reciba las coordenadas rectangulares de dos puntos 
del plano y devuelva la distancia entre ellos. 
b. Vali�ndose de la funci�n del punto anterior, escribir una funci�n que calcule 
la longitud total de un segmento de n puntos bidimensionales, respondiendo al 
siguiente prototipo: 
double longitud_segmento(double puntos[][2], size_t n);*/


#include <stdio.h>
#include <math.h>

double distancia (double x1, double y1, double x2, double y2)
{
	return sqrt(powf((x2-x1),2) + powf((y2-y1), 2));
}

double longitud_segmento(double puntos[][2], size_t n)
{
	int i;
	double l=0;
	
	for (i= 0; i<n-1; i++)
	{
		l+= distancia(puntos[i][0], puntos[i][1], puntos[i+1][0], puntos[i+1][1]);
	}
	return l;
}

int main(int argc, char *argv[]) {
	double matriz[3][2]={0, 1, 2, 4, 3, 8};
	
	double d= longitud_segmento(matriz, 3);
	printf ("Longitud total: %f", d);
	
	return 0;
}

