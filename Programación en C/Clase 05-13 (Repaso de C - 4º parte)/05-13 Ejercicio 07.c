/* 13 de mayo ejercicio 7
Sea A una matriz de tama�o nxn, implemente un programa que dado un men� de 
opciones resuelva: � La transpuesta de A (At). � Si A es sim�trica o 
antisim�trica. � Si A es una matriz triangular superior o triangular inferior.*/

#include <stdio.h>
#include <math.h>

void impresion (int [][50], int, int);
void traspuesta (int [][50], int, int);

int main(int argc, char *argv[]) {
	int fila, col;
	int triang_inf= 1, triang_sup= 1;
	int A[50][50];
	int i, j;

	printf ("Ingrese el numero de filas: ");
	scanf ("%d", &fila);
	printf ("Ingrese el numero de columnas: ");
	scanf ("%d", &col);
	printf("\n");
	for (i=0; i<fila; i++)
	{
		for (j=0; j<col; j++)
		{
			printf ("Ingrese el elemento %d:%d: ", i, j);
			scanf ("%d", &A[i][j]);
			if (i< j && A[i][j] != 0)
			{
				triang_inf= 0;
			}
			if (i > j && A[i][j] != 0)
			{
				triang_sup= 0;
			}
		}
	}
	
	printf ("\nMatriz original:\n");
	impresion(A, fila, col);

	traspuesta (A, fila, col);
	printf ("\n\nMatriz traspuesta:\n");
	impresion(A, col, fila);
	
	printf ("\n\nCaracter%cstica de la matriz original:\n", 161);
	if (fila == col)
	{
		printf("\n- Sim%ctrica", 130);
	}
	else
	{
		printf("\n- Asim%ctrica", 130);
	}
	if (triang_inf == 1)
	{
		printf("\n- Triangular inferior.");
	}
	if (triang_sup == 1)
	{
		printf("\n- Triangular superior.");
	}
	if (triang_inf == 0 && triang_sup == 0)
	{
		printf("\n- No posee triangularidad.");
	}
	
	return 0;
}

void impresion (int A[][50], int fila, int col)
{
	int i, j;
	
	for (i=0; i<fila; i++)
	{
		printf("\n");
		for (j=0; j<col; j++)
		{
			printf ("%d\t", A[i][j]);
		}
	}
}

void traspuesta (int A[][50], int fila, int col)
{
	int aux;
	int i, j;
	int tope;
	
	if (fila > col)
		tope= fila;
	else
		tope= col;
	
	for (i=0; i<fila; i++)
	{
		for (j=0; j<tope; j++)
		{
			if (i != j && i<j)
			{
				aux= A[j][i];
				A[j][i]= A[i][j];
				A[i][j]= aux;
			}
		}
	}
}
