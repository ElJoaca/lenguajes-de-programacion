/* 13 de mayo ejercicio 9
Crear una funci�n que determine cu�l es el centro de una cadena y devuelva la 
mitad derecha de la misma.*/

#include <stdio.h>

int main(int argc, char *argv[]) {
	int count= -1;
	int i=0;
	char s1[30];
	char s2[30]={0};
	
	printf ("Ingrese una frase: ");
	fgets (s1, 30, stdin);
	while (s1[i] != '\0')
	{
		count += 1;
		i++;
	}
	printf ("\nSe ingresaron %d caracteres.\n", count);
	for (i=0; i<count/2; i++)
	{
		s2[i]=s1[(count/2)+i];
	}
	printf("\nMitad derecha de la cadena: %s", s2);
	return 0;
}
