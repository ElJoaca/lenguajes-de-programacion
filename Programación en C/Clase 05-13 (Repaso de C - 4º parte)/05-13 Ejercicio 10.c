/* 13 de mayo ejercicio 10
Crear una funci�n que concatena dos cadenas (cadena 1 y cadena 2) dejando el 
resultado de dicha operaci�n sobre la primera (cadena 1).*/

#include <stdio.h>

int main(int argc, char *argv[]) {
	int count1= -1;
	int count2= -1;
	int i=0, j=0;
	char s1[30]={0};
	char s2[30]={0};
	
	printf ("Ingrese la primer frase: ");
	fgets (s1, 30, stdin);
	printf ("Ingrese la segunda frase: ");
	fgets (s2, 30, stdin);
	while (s1[i] != '\0')
	{
		count1 += 1;
		i++;
	}
	while (s2[j] != '\0')
	{
		count2 += 1;
		j++;
	}
	printf ("\nEn la primera frase se ingresaron %d caracteres.\n", count1);
	printf ("En la segunda frase se ingresaron %d caracteres.\n", count2);
	for (i=0; i<count2; i++)
	{
		s1[count1+i]=s2[i];
	}
	printf("\nCadenas concatenadas: %s", s1);
	return 0;
}
