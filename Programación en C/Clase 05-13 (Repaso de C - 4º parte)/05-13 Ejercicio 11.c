/* 13 de mayo ejercicio 11
A partir de la declaraci�n de las siguientes estructuras (ver PDF) realice un 
programa en C que lea el array �ats� y devuelva los datos (nombre, pa�s, 
deporte) del atleta que ha ganado mayor numero de medallas.*/

#include <stdio.h>

struct datos
{
	char nombre[40];
	char pais[25];
};

struct atleta
{
	char deporte[30];
	struct datos pers;
	int nmedallas;
};


int main(int argc, char *argv[]) {
	int n;
	int max_medallas= 0;
	int i;
	struct atleta ats[30];
	
	printf ("Ingrese la cantidad de atletas: ");
	scanf ("%d", &n);
	for (i=0; i<n; i++)
	{
		printf ("\nIngrese el nombre del atleta N� %d: ", i+1);
		scanf ("%s", &ats[i].pers.nombre);
		printf ("Ingrese el pa�s de correspondencia: ");
		scanf ("%s", &ats[i].pers.pais);
		printf ("Ingrese el deporte: ");
		scanf ("%s", &ats[i].deporte);
		printf ("Ingrese el n%cmero de medallas conseguidas: ", 163);
		scanf ("%d", &ats[i].nmedallas);
		if (ats[i].nmedallas > ats[max_medallas].nmedallas)
		{
			max_medallas= i;
		}
	}
	printf ("\n%s es un profesional de %s, procedente de %s y es el que m%cs medallas consigui%c: %d", ats[max_medallas].pers.nombre, ats[max_medallas].deporte, ats[max_medallas].pers.pais, 160, 162, ats[max_medallas].nmedallas);
	
	return 0;
}

