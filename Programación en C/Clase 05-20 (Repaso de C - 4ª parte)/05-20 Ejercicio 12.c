/* 30 de mayo ejercicio 12
Defina la estructura tiempo que permita almacenar valores de tiempo expresados 
en horas, minutos y segundos. Luego, realice un programa que permita cargar un 
listado de valores de tiempo introducidos por teclado. El programa deber� 
solicitar al usuario el n�mero de valores a recibir y reservar de manera 
din�mica la memoria para almacenarlos. Finalmente el programa deber� calcular y 
mostrar en pantalla el tiempo total dado por la suma de todos los valores 
ingresados. */

#include <stdio.h>
#include <stdlib.h>

struct tiempo
{
	int horas;
	int minutos;
	int segundos;
};

int main(int argc, char *argv[]) {
	
	int n= 0;
	int i= 0;
	struct tiempo *tiempos;
	struct tiempo suma= {0};
	
	printf("Ingrese cantidad de valores: ");
	scanf("%d", &n);
	tiempos= (struct tiempo*) malloc(n*sizeof(struct tiempo));
	if (tiempos != NULL)
	{
		printf("\nIngrese datos: \n");
		for (i=0; i<n; i++)
		{
			printf("\nIngrese hora: ");
			scanf("%d", &tiempos[i].horas);
			printf("Ingrese minutos: ");											
			scanf("%d", &tiempos[i].minutos);									
			printf("Ingrese segundos: ");
			scanf("%d", &tiempos[i].segundos);
			suma.horas += tiempos[i].horas;
			suma.minutos += tiempos[i].minutos;
			suma.segundos += tiempos[i].segundos;
		}
		while(suma.segundos >= 60)
		{
			suma.segundos -=60;
			suma.minutos +=1;
		}
		while(suma.minutos >= 60)
		{
			suma.minutos -=60;
			suma.horas +=1;
		}
		free(tiempos);
		printf ("\nTiempo total %d horas, %d minutos, %d segundos (%d:%d:%d).", suma.horas, suma.minutos, suma.segundos, suma.horas, suma.minutos, suma.segundos);
	}
	else
	{
		return 1;	
	}
	
	return 0;
}
