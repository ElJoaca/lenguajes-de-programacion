/* 27 de mayo ejercicio 13
Desarrolle un programa en lenguaje C que incluya lo siguiente:
1- Definici�n de una estructura adecuada para almacenar datos de un alumno 
(nombre, apellido, legajo, notas, condici�n). La condici�n de los alumnos ser�: 
LIBRE, REGULAR, PROMOCION PRACTICA, PROMOCION TEORICA, APROBACION DIRECTA. Este 
dato no ser� cargado por el usuario, sino que ser� determinado de manera 
autom�tica por el programa. Utilice como gu�a el reglamento de la catedra 
Inform�tica II.
2- Permitir cargar una lista de alumnos con sus datos y las notas 
correspondientes a una determinada asignatura en un arreglo de la estructura 
definida en 1. Debe implementarse algun mecanismo (mediante men�) para permitir 
al usuario cargar los alumnos y detener la carga en cualquier momento. Se podr�n 
cargar como m�ximo 50 alumnos.
3- Deber� ofrecer la posibilidad de modificar los datos de cualquier alumno que 
ya haya sido cargado.
4- Finalizada la carga, el programa deber� ofrecer la opci�n de mostrar en 
pantalla el listado completo de alumnos y sus datos o mostrar un resumen 
indicando el porcentaje de los alumnos en cada condici�n.
5- Utilizar comentarios para explicar el funcionamiento de cada funcion y 
otras partes relevantes del c�digo implementado.
6- Implementar los controles que considere necesario tales como verificar 
valores validos en la carga de datos, permitiendo re introducirlos tras un 
error.*/
/*Actualizaciones:
Uso de enum y la funci�n GetString para convertirl la condici�n en una cadena de
caracteres*/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

enum opciones {Ingresar_un_nuevo_alumno= 49, Modificar_un_alumno_existente= 50, 
	Lista_de_alumnos= 51, Finalizar_el_programa= 27};

enum condicion {Libre, Regular, Promocion_practica, Promocion_Teorica, 
	Aprobacion_Directa};

struct datos {
	char nombre [40];
	char apellido [40];
	int legajo;
	int notas [4];																//6
} alumno[51];

char *GetString (int);

int main(int argc, char *argv[]) {
	int bandera;
	int i= 1, j, k;
	char opcion;
	char buffer;
	
	do
	{
		system ("cls");
		printf ("Alumnos inform%ctica II:\n", 160);
		printf ("\n1) Ingresar un nuevo alumno.");
		printf ("\n2) Modificar un alumno existente.");
		printf ("\n3) Lista de alumnos.");
		printf ("\nesc) Finalizar el programa.");
		printf ("\n\nIngresar opci%cn: ", 162);
		opcion= getch();
		
		switch (opcion)
		{
		case Ingresar_un_nuevo_alumno:
			system ("cls");
			printf ("Ingresar un nuevo alumno:");
			printf ("\n\n[%d] Ingrese el nombre: ", i);
			scanf ("%s", &alumno[i].nombre);
			printf ("[%d] Ingrese el apellido: ", i);
			scanf ("%s", &alumno[i].apellido);
			
			// Legajo //
			printf ("[%d] Ingrese el legajo: ", i);
			bandera= scanf ("%d", &alumno[i].legajo);
			while(bandera != 0)
			{
				system ("cls");
				printf ("ERROR fatal:");
				printf ("\n\n%cLa opci%cn ingresada no es v%clida.\n\n", 07, 162, 160);
				system ("pause");
				system ("cls");
				printf ("[%d] Reingrese el legajo: ", i);
				while ((buffer = getchar()) != '\n' && buffer != EOF);			//Limpiar buffer de entrada.
				bandera= scanf ("%d", &alumno[i].legajo);
			}
			
			// 1er parcial Pr�ctico //
			printf ("[%d] Ingrese la nota del primer parcial practico: ", i);
			scanf ("%d", &alumno[i].notas[0]);
			while (alumno[i].notas[0] < 1 || alumno[i].notas[0] > 10)
			{
				system ("cls");
				printf ("ERROR fatal:");
				printf ("\n\n%cLa opci%cn ingresada no es v%clida.\n\n", 07, 162, 160);
				system ("pause");
				system ("cls");
				printf ("[%d] Reingrese la nota del primer parcial practico: ", i);
				scanf ("%d", &alumno[i].notas[0]);
			}
			
			// 2do parcial Pr�ctico //
			printf ("[%d] Ingrese la nota del segundo parcial practico: ", i);
			scanf ("%d", &alumno[i].notas[1]);
			while (alumno[i].notas[1] < 1 || alumno[i].notas[1] > 10)
			{
				system ("cls");
				printf ("ERROR fatal:");
				printf ("\n\n%cLa opci%cn ingresada no es v%clida.\n\n", 07, 162, 160);
				system ("pause");
				system ("cls");
				printf ("[%d] Reingrese la nota del segundo parcial practico: ", i);
				scanf ("%d", &alumno[i].notas[1]);
			}
			
			// 1er parcial Te�rico //
			printf ("[%d] Ingrese la nota del primer parcial te%crico: ", i, 162);
			scanf ("%d", &alumno[i].notas[2]);
			while (alumno[i].notas[2] < 1 || alumno[i].notas[2] > 10)
			{
				system ("cls");
				printf ("ERROR fatal:");
				printf ("\n\n%cLa opci%cn ingresada no es v%clida.\n\n", 07, 162, 160);
				system ("pause");
				system ("cls");
				printf ("[%d] Reingrese la nota del primer parcial te%crico: ", i, 162);
				scanf ("%d", &alumno[i].notas[2]);
			}
			
			// 2do parcial Te�rico //
			printf ("[%d] Ingrese la nota del segundo parcial te%crico: ", i, 162);
			scanf ("%d", &alumno[i].notas[3]);
			while (alumno[i].notas[3] < 1 || alumno[i].notas[3] > 10)
			{
				system ("cls");
				printf ("ERROR fatal:");
				printf ("\n\n%cLa opci%cn ingresada no es v%clida.\n\n", 07, 162, 160);
				system ("pause");
				system ("cls");
				printf ("[%d] Reingrese la nota del segundo parcial te%crico: ", i, 162);
				scanf ("%d", &alumno[i].notas[3]);
			}
			printf ("\n");
			system ("pause");
			i++;
			break;
		case Modificar_un_alumno_existente:
			system ("cls");
			printf ("Modificar un alumno existente:");
			printf ("\n\nIngrese el numero de alumno que desea modificar: ");
			scanf ("%d", &j);
			if (j < 1 || j > 50)
			{
				system ("cls");
				printf ("ERROR fatal:");
				printf ("\n\n%cLa opci%cn ingresada no es v%clida.\n\n", 07, 162, 160);
				system ("pause");
			}
			else
			{
				printf ("\n[%d] Ingrese el nombre: ", i);
				scanf ("%s", &alumno[j].nombre);
				printf ("[%d] Ingrese el apellido: ", i);
				scanf ("%s", &alumno[j].apellido);
				printf ("[%d] Ingrese el legajo: ", i);
				
				// Legajo //
				bandera= scanf ("%d", &alumno[j].legajo);
				while(bandera != 1)
				{
					system ("cls");
					printf ("ERROR fatal:");
					printf ("\n\n%cLa opci%cn ingresada no es v%clida.\n\n", 07, 162, 160);
					system ("pause");
					system ("cls");
					printf ("[%d] Reingrese el legajo: ", j);
					while ((buffer = getchar()) != '\n' && buffer != EOF);		//Limpiar buffer de entrada.
					bandera= scanf ("%d", &alumno[j].legajo);
				}
				
				// 1er parcial Pr�ctico //
				printf ("[%d] Ingrese la nota del primer parcial practico: ", i);
				scanf ("%d", &alumno[j].notas[0]);
				while (alumno[j].notas[0] < 1 || alumno[j].notas[0] > 10)
				{
					system ("cls");
					printf ("ERROR fatal:");
					printf ("\n\n%cLa opci%cn ingresada no es v%clida.\n\n", 07, 162, 160);
					system ("pause");
					system ("cls");
					printf ("[%d] Reingrese la nota del primer parcial practico: ", j);
					scanf ("%d", &alumno[j].notas[0]);
				}
				
				// 2do parcial Pr�ctico //
				printf ("[%d] Ingrese la nota del segundo parcial practico: ", i);
				scanf ("%d", &alumno[j].notas[1]);
				while (alumno[j].notas[1] < 1 || alumno[j].notas[1] > 10)
				{
					system ("cls");
					printf ("ERROR fatal:");
					printf ("\n\n%cLa opci%cn ingresada no es v%clida.\n\n", 07, 162, 160);
					system ("pause");
					system ("cls");
					printf ("[%d] Reingrese la nota del segundo parcial practico: ", j);
					scanf ("%d", &alumno[j].notas[1]);
				}
				
				// 1er parcial Te�rico //
				printf ("[%d] Ingrese la nota del primer parcial te%crico: ", i, 162);
				scanf ("%d", &alumno[j].notas[2]);
				while (alumno[j].notas[2] < 1 || alumno[j].notas[2] > 10)
				{
					system ("cls");
					printf ("ERROR fatal:");
					printf ("\n\n%cLa opci%cn ingresada no es v%clida.\n\n", 07, 162, 160);
					system ("pause");
					system ("cls");
					printf ("[%d] Reingrese la nota del primer parcial te%crico: ", j, 162);
					scanf ("%d", &alumno[j].notas[2]);
				}
				
				// 2do parcial Te�rico //
				printf ("[%d] Ingrese la nota del segundo parcial te%crico: ", i, 162);
				scanf ("%d", &alumno[j].notas[3]);
				while (alumno[j].notas[3] < 1 || alumno[j].notas[3] > 10)
				{
					system ("cls");
					printf ("ERROR fatal:");
					printf ("\n\n%cLa opci%cn ingresada no es v%clida.\n\n", 07, 162, 160);
					system ("pause");
					system ("cls");
					printf ("[%d] Reingrese la nota del segundo parcial te%crico: ", j, 162);
					scanf ("%d", &alumno[j].notas[3]);
				}
				
				printf ("\n");
				system ("pause");
			}
			break;
		case Lista_de_alumnos:																//Listar alumnos.
			system ("cls");
			printf("Lista de alumnos:");
			printf ("\n\n|Legajo|     |Notas pr%ctico|     |Notas te%crico|     |Nombre y Apellido|     |Estado acad%cmico|\n", 160, 162, 130);
			for (k=1; k<i; k++)
			{
				printf ("\n[%d]           %d - %d               %d - %d          %s, %s", alumno[k].legajo, alumno[k].notas[0], alumno[k].notas[1], alumno[k].notas[2], alumno[k].notas[3], alumno[k].apellido, alumno[k].nombre);
				if (alumno[k].notas[0] < 4 && alumno[k].notas[1] < 4)
				{
					printf (" -> %s", GetString(Libre));
				}
				if (alumno[k].notas[0] >= 4 && alumno[k].notas[0] < 7 && alumno[k].notas[1] >= 4 && alumno[k].notas[1] < 7 && (alumno[k].notas[2] < 7 || alumno[k].notas[3] < 7))
				{
					printf (" -> %s", GetString(Regular));
				}
				if (alumno[k].notas[0] >= 7 && alumno[k].notas[1] >= 7 && (alumno[k].notas[2] < 7 || alumno[k].notas[3] < 7))
				{
					printf (" -> %s", GetString(Promocion_practica));
				}
				if (alumno[k].notas[0] >= 4 && alumno[k].notas[0] < 7 && alumno[k].notas[1] >= 4 && alumno[k].notas[1] < 7 && alumno[k].notas[2] >= 7 && alumno[k].notas[3] >= 7)
				{
					printf (" -> %s", GetString(Promocion_Teorica));
				}
				if (alumno[k].notas[0] >= 7 && alumno[k].notas[1] >= 7 && alumno[k].notas[2] >= 7 && alumno[k].notas[3] >= 7)
				{
					printf (" -> %s", GetString(Aprobacion_Directa));
				}
			}
			printf ("\n\n");
			system ("pause");
			break;
		case Finalizar_el_programa:												//Finalizar el programa.
			system ("cls");
			printf ("Salir:");
			printf ("\n\nPrograma finalizado correctamente.\n\n");
			system ("pause");
			break;
		default:
			system ("cls");
			printf ("ERROR fatal:");
			printf ("\n\n%cLa opci%cn ingresada no es v%clida.\n\n", 07, 162, 160);
			system ("pause");
			break;
		}
		
	} while(opcion != 27);
	
	return 0;
}

char *GetString (int cond)
{
	switch (cond)
	{
		case Libre:
			return "Libre";
			break;
		case Regular:
			return "Regular";
			break;
		case Promocion_practica:
			return "Promocion-practica";
			break;
		case Promocion_Teorica:
			return "Promocion-Teorica";
			break;
		case Aprobacion_Directa:
			return "Aprobacion-Directa";
			break;
		default:
			return "Error";
	}
}
