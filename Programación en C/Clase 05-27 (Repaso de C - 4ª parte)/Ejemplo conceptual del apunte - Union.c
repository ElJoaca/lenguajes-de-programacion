/* 27 de mayo ejemplo union*/

#include <stdio.h>
#include <stdlib.h>

union entero {
	int i;
	float f;
	char c[4];
};

int main(int argc, char *argv[]) {
	union entero u1;
	
	u1.i= 1500000000;
	printf("Numero: %d\n\n", u1.f);
	//u1.f= 3.141592;
	//printf("Numero: %f\n\n", u1.f);
	printf("Hexa: %x\n", u1.i);
	printf("byte 0: %x\n", u1.c[0]);
	printf("byte 1: %x\n", u1.c[1]);
	printf("byte 2: %x\n", u1.c[2]);
	printf("byte 3: %x\n\n", u1.c[3]);
	printf("Direccion de la union: %p\n", &u1);
	printf("Direccion de entero: %p\n", &u1.i);
	printf("Direccion de char: %p\n", &u1.c);
	
	return 0;
}

