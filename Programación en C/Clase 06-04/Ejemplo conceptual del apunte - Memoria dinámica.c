/* 4 de junio ejemplo memoria din�mica*/

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
	
	printf ("Hola Mundo!\n");
	
	// Declaraci�n de un vector de enteros cuyo tama�o es solicitado al usuario en tiempo de ejecuci�n.
	
	// Declaro un puntero a entero para manejar el vector
	
	int *vector;
	
	// Variable para almacenar el tama�o.
	
	int N= 0;
	
	printf ("Ingrese tama�o del vector: ");
	scanf ("%d", &N);
	
	// Reservo memoria con la funci�n malloc:
	
	vector= (int*) malloc (N*sizeof(int));
	
	// O reservo memoria con la funci�n calloc (inicializa la regi�n de memoria a cero):
	
	// Vector= (int *) calloc (N, sizeof (int));
	
	if (vector == NULL)
	{
		printf ("Erro!");
		return 1; // Error
	}
	else
	{
		printf ("Direccion otorgada: %p\n", vector);
		
		// Si deseo inicializar a cero el vector:
		
		for (int i=0; i<N; i++)
		{
			vector [i]= 0;
		}
		
		// Si deseo redimensionar el vector (m�s o menos elementos)
		
		printf ("Ingrese nuevo tama�o del vector: ");
		scanf ("%d", &N);
		vector= (int *) realloc (vector, N);
		if (vector == NULL)
		{
			printf ("ERROR!");
			return 1; // Error
		}
	}
	
	return 0;
}

