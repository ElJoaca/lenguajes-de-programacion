/* 10 de junio ejemplo 1
Abrir el archivo de texto ejemplo.txt en modo lectura e imprimir su contenido
en consola*/

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
	FILE *archivo;
	char caracter;
	char cadena[100]= {0};
	
	archivo= fopen("06-10 Ejemplo 1 del apunte - Archivos.txt", "r");			//Apertura del archivo en modo lectura.
	
	if (archivo == NULL)
	{
		printf("\nError de apertura del archivo. \n\n");
	}
	else
	{
		printf("\nEl contenido del archivo de prueba es \n\n");
		while((caracter = fgetc(archivo)) != EOF)								//Lectura caracter por caracter hasta el fin de archivo EOF.
		{
			printf("%c", caracter);
		}
		
		fseek(archivo, 0, SEEK_SET);											//Vuelvo al inicio del archivo.
		
		fscanf (archivo, "%s", cadena);											//Lectura con funcion fscanf.
		printf("\nContenido del archivo:\n%s\n", cadena);
		
		fclose(archivo);														//Cierre del archivo.
	}

	return 0;
}

