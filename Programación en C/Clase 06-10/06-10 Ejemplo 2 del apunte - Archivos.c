/* 10 de junio ejemplo 2
Crear el archivo de texto prueba2.txt y guardar en el mismo un texto introducido 
por consola:*/

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
	FILE *fp;
	
	char cadena[100]={0};
	
	fp= fopen("06-10 Ejemplo 2 del apunte - Archivos.txt", "w+");									//Crea el archivo y si existe lo va a reemplazar.
	if (fp == NULL)
	{
		printf("\nError de apertura del archivo. \n\n");
	}
	else
	{
		printf("Ingrese texto a guardar: ");
		scanf("%s", cadena);
		fputs(cadena, fp);
		fclose(fp);
	}
	return 0;
}

