/* 10 de junio ejemplo 3
Abrir el archivo prueba3.txt que contiene 10 n�meros con decimales, uno en cada 
rengl�n, calcular el promedio y mostrar todo en pantalla:*/

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
	FILE *archivo;
	
	int i;
	float v[10]={0};
	float promedio=0;

	archivo= fopen("06-10 Ejemplo 3 del apunte - Archivos.txt", "r");			//Apertura del archivo en modo lectura.
	if (archivo == NULL)
	{
		printf("\nError de apertura del archivo. \n\n");
	}
	else
	{
		printf("\nDatos:\n");
		for (i=0; (i<10) && (!feof(archivo)); i++)
		{
			fscanf(archivo, "%f", &v[i]);										//Lectura con funcion fscanf.
			promedio += v[i];
			printf("%f, ", v[i]);
		}
		promedio /= (float)i;
		printf("\nPromedio de %d nros: %f\n", i, promedio);
		fclose(archivo);														//Cierre del archivo.
	}
	
	return 0;
}
