/* 10 de junio ejemplo 4
En el archivo anterior prueba3.txt modifique el 4� numero:*/

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
	FILE *archivo;
	
	int i;
	float v[10]={0};
	float promedio=0;
	
	archivo= fopen("06-10 Ejemplo 4 del apunte - Archivos.txt", "r+");			//Apertura del archivo en modo lectura.
	if (archivo == NULL)
	{
		printf("\nError de apertura del archivo. \n\n");
	}
	else
	{
		printf("\nDatos:\n");
		for (i=0; (i<10) && (!feof(archivo)); i++)
		{
			fscanf(archivo, "%f", &v[i]);										//Lectura con funcion fscanf.
			promedio += v[i];
			printf("%f, ", v[i]);
		}
		promedio /= (float)i;
		printf("\nPromedio de %d nros: %f\n", i, promedio);
		
		fseek(archivo,3*(6)-1,SEEK_SET);										//Modifico el 4� nro. El 3 es por la posici�n (me da el 4to rengl�n porque incluye la posici�n 0). El 6 es por cada l�nea (contando el \n\r).
		int n= fprintf(archivo,"%1.2f", 5.20f);									
		printf("\nCantidad de bytes escritos: %d", n);
		
		rewind(archivo);														//Vuelvo a mostrar el contenido.
		printf("\nDatos:\n");
		for (i=0; (i<10) && (!feof(archivo)); i++)
		{
			fscanf(archivo, "%f", &v[i]);										//Lectura con funcion fscanf.
			promedio += v[i];
			printf("%f, ", v[i]);
		}
		promedio /= (float)i;
		printf("\nPromedio de %d: %f\n",i ,promedio);
		fclose(archivo);
	}
	
	return 0;
}
