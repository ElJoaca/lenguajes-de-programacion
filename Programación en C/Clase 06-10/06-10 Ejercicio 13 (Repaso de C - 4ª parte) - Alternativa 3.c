/* 10 de junio ejercicio 13
Desarrolle un programa en lenguaje C que incluya lo siguiente:
1- Definici�n de una estructura adecuada para almacenar datos de un alumno 
(nombre, apellido, legajo, notas, condici�n). La condici�n de los alumnos ser�: 
LIBRE, REGULAR, PROMOCION PRACTICA, PROMOCION TEORICA, APROBACION DIRECTA. Este 
dato no ser� cargado por el usuario, sino que ser� determinado de manera 
autom�tica por el programa. Utilice como gu�a el reglamento de la catedra 
Inform�tica II.
2- Permitir cargar una lista de alumnos con sus datos y las notas 
correspondientes a una determinada asignatura en un arreglo de la estructura 
definida en 1. Debe implementarse algun mecanismo (mediante men�) para permitir 
al usuario cargar los alumnos y detener la carga en cualquier momento. Se podr�n 
cargar como m�ximo 50 alumnos.
3- Deber� ofrecer la posibilidad de modificar los datos de cualquier alumno que 
ya haya sido cargado.
4- Finalizada la carga, el programa deber� ofrecer la opci�n de mostrar en 
pantalla el listado completo de alumnos y sus datos o mostrar un resumen 
indicando el porcentaje de los alumnos en cada condici�n.
5- Utilizar comentarios para explicar el funcionamiento de cada funcion y 
otras partes relevantes del c�digo implementado.
6- Implementar los controles que considere necesario tales como verificar 
valores validos en la carga de datos, permitiendo re introducirlos tras un 
error.*/
/*Actualizaciones:
Uso de typedef. Uso de malloc, ahora se le pregunta al usuario cuantos alumnos 
desea ingresar.*/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

enum opciones {Ingresar_un_nuevo_alumno= 49, Modificar_un_alumno_existente= 50, 
	Lista_de_alumnos= 51, Finalizar_el_programa= 27};

enum condicion {Libre, Regular, Promocion_practica, Promocion_Teorica, 
	Aprobacion_Directa};

typedef struct datos {
	char nombre [40];
	char apellido [40];
	int legajo;
	int notas [4];																//6
} alumno;

char *GetString (int);

int main(int argc, char *argv[]) {
	int n;
	int bandera;
	int i= 1, j, k= 0;
	char opcion;
	char buffer;
	alumno *curso;

	printf ("Ingrese la cantidad de alumnos que desea cargar: ");
	scanf ("%d", &n);
	curso= (alumno *) malloc (n*sizeof(alumno));
	printf ("\n");
	system ("pause");
	if (curso == NULL)
	{
		system ("cls");
		printf ("%cERROR fatal:\n\n", 07);
		system ("pause");
	}
	else
	{
		do
		{
			system ("cls");
			printf ("Alumnos inform%ctica II (%d alumnos):\n", 160, n);
			printf ("\n1) Ingresar un nuevo alumno.");
			printf ("\n2) Modificar un alumno existente.");
			printf ("\n3) Lista de alumnos.");
			printf ("\nesc) Finalizar el programa.");
			printf ("\n\nIngresar opcion: ");
			opcion= getch();
			
			switch (opcion)
			{
			case Ingresar_un_nuevo_alumno:
				// Nombre //
				system ("cls");
				printf ("Ingresar un nuevo alumno:");
				printf ("\n\n[%d] Ingrese el nombre: ", i);
				scanf ("%s", &curso[i].nombre);
				while (curso[i].nombre[k] != '\0')
				{
					if ((curso[i].nombre[k] < 65) || (curso[i].nombre[k] > 90 && curso[i].nombre[k] < 97) || (curso[i].nombre[k] > 122))
					{
						bandera= 0;
					}
					k++;
				}
				k= 0;
				while (bandera == 0)
				{
					system ("cls");
					printf ("ERROR fatal:");
					printf ("\n\n%cEl nombre ingresado no es v%clido.\n\n", 07, 160);
					system ("pause");
					system ("cls");
					printf ("[%d] Reingrese el nombre: ", i);
					scanf ("%s", &curso[i].nombre);
					while (curso[i].nombre[k] != '\0')
					{
						if ((curso[i].nombre[k] < 65) || (curso[i].nombre[k] > 90 && curso[i].nombre[k] < 97) || (curso[i].nombre[k] > 122))
						{
							bandera= 0;
						}
						else
						{
							bandera= 1;
						}
						k++;
					}
				}
				k= 0;
				
				// Apellido //
				printf ("[%d] Ingrese el apellido: ", i);
				scanf ("%s", &curso[i].apellido);
				while (curso[i].apellido[k] != '\0')
				{
					if ((curso[i].apellido[k] < 65) || (curso[i].apellido[k] > 90 && curso[i].apellido[k] < 97) || (curso[i].apellido[k] > 122))
					{
						bandera= 0;
					}
					k++;
				}
				k= 0;
				while (bandera == 0)
				{
					system ("cls");
					printf ("ERROR fatal:");
					printf ("\n\n%cEl apellido ingresado no es v%clido.\n\n", 07, 160);
					system ("pause");
					system ("cls");
					printf ("[%d] Reingrese el apellido: ", i);
					scanf ("%s", &curso[i].apellido);
					while (curso[i].apellido[k] != '\0')
					{
						if ((curso[i].apellido[k] < 65) || (curso[i].apellido[k] > 90 && curso[i].apellido[k] < 97) || (curso[i].apellido[k] > 122))
						{
							bandera= 0;
						}
						else
						{
							bandera= 1;
						}
						k++;
					}
				}
				k= 0;
				
				// Legajo //
				printf ("[%d] Ingrese el legajo: ", i);
				bandera= scanf ("%d", &curso[i].legajo);
				while(bandera != 1)
				{
					system ("cls");
					printf ("ERROR fatal:");
					printf ("\n\n%cLa opci%cn ingresada no es v%clida.\n\n", 07, 162, 160);
					system ("pause");
					system ("cls");
					printf ("[%d] Reingrese el legajo: ", i);
					while ((buffer = getchar()) != '\n' && buffer != EOF);		//Limpiar buffer de entrada.
					bandera= scanf ("%d", &curso[i].legajo);
				}
				
				// 1er parcial Pr�ctico //
				printf ("[%d] Ingrese la nota del primer parcial practico: ", i);
				scanf ("%d", &curso[i].notas[0]);
				while (curso[i].notas[0] < 1 || curso[i].notas[0] > 10)
				{
					system ("cls");
					printf ("ERROR fatal:");
					printf ("\n\n%cLa opci%cn ingresada no es v%clida.\n\n", 07, 162, 160);
					system ("pause");
					system ("cls");
					printf ("[%d] Reingrese la nota del primer parcial practico: ", i);
					scanf ("%d", &curso[i].notas[0]);
				}
				
				// 2do parcial Pr�ctico //
				printf ("[%d] Ingrese la nota del segundo parcial practico: ", i);
				scanf ("%d", &curso[i].notas[1]);
				while (curso[i].notas[1] < 1 || curso[i].notas[1] > 10)
				{
					system ("cls");
					printf ("ERROR fatal:");
					printf ("\n\n%cLa opci%cn ingresada no es v%clida.\n\n", 07, 162, 160);
					system ("pause");
					system ("cls");
					printf ("[%d] Reingrese la nota del segundo parcial practico: ", i);
					scanf ("%d", &curso[i].notas[1]);
				}
				
				// 1er parcial Te�rico //
				printf ("[%d] Ingrese la nota del primer parcial te%crico: ", i, 162);
				scanf ("%d", &curso[i].notas[2]);
				while (curso[i].notas[2] < 1 || curso[i].notas[2] > 10)
				{
					system ("cls");
					printf ("ERROR fatal:");
					printf ("\n\n%cLa opci%cn ingresada no es v%clida.\n\n", 07, 162, 160);
					system ("pause");
					system ("cls");
					printf ("[%d] Reingrese la nota del primer parcial te%crico: ", i, 162);
					scanf ("%d", &curso[i].notas[2]);
				}
				
				// 2do parcial Te�rico //
				printf ("[%d] Ingrese la nota del segundo parcial te%crico: ", i, 162);
				scanf ("%d", &curso[i].notas[3]);
				while (curso[i].notas[3] < 1 || curso[i].notas[3] > 10)
				{
					system ("cls");
					printf ("ERROR fatal:");
					printf ("\n\n%cLa opci%cn ingresada no es v%clida.\n\n", 07, 162, 160);
					system ("pause");
					system ("cls");
					printf ("[%d] Reingrese la nota del segundo parcial te%crico: ", i, 162);
					scanf ("%d", &curso[i].notas[3]);
				}
				printf ("\n");
				system ("pause");
				i++;
				break;
			case Modificar_un_alumno_existente:
				// Nombre //
				system ("cls");
				printf ("Modificar un alumno existente:");
				printf ("\n\nIngrese el numero de alumno que desea modificar: ");
				scanf ("%d", &j);
				if (j < 1 || j > n)
				{
					system ("cls");
					printf ("ERROR fatal:");
					printf ("\n\n%cLa opci%cn ingresada no es v%clida.\n\n", 07, 162, 160);
					system ("pause");
				}
				else
				{
					printf ("\n[%d] Ingrese el nombre: ", j);
					scanf ("%s", &curso[j].nombre);
					while (curso[j].nombre[k] != '\0')
					{
						if ((curso[j].nombre[k] < 65) || (curso[j].nombre[k] > 90 && curso[j].nombre[k] < 97) || (curso[j].nombre[k] > 122))
						{
							bandera= 0;
						}
						k++;
					}
					k= 0;
					while (bandera == 0)
					{
						system ("cls");
						printf ("ERROR fatal:");
						printf ("\n\n%cEl nombre ingresado no es v%clido.\n\n", 07, 160);
						system ("pause");
						system ("cls");
						printf ("[%d] Reingrese el nombre: ", j);
						scanf ("%s", &curso[j].nombre);
						while (curso[j].nombre[k] != '\0')
						{
							if ((curso[j].nombre[k] < 65) || (curso[j].nombre[k] > 90 && curso[j].nombre[k] < 97) || (curso[j].nombre[k] > 122))
							{
								bandera= 0;
							}
							else
							{
								bandera= 1;
							}
							k++;
						}
					}
					k= 0;
					
					// Apellido //
					printf ("[%d] Ingrese el apellido: ", j);
					scanf ("%s", &curso[j].apellido);
					while (curso[j].apellido[k] != '\0')
					{
						if ((curso[j].apellido[k] < 65) || (curso[j].apellido[k] > 90 && curso[j].apellido[k] < 97) || (curso[j].apellido[k] > 122))
						{
							bandera= 0;
						}
						k++;
					}
					k= 0;
					while (bandera == 0)
					{
						system ("cls");
						printf ("ERROR fatal:");
						printf ("\n\n%cEl apellido ingresado no es v%clido.\n\n", 07, 160);
						system ("pause");
						system ("cls");
						printf ("[%d] Reingrese el apellido: ", k);
						scanf ("%s", &curso[j].apellido);
						while (curso[j].apellido[k] != '\0')
						{
							if ((curso[j].apellido[k] < 65) || (curso[j].apellido[k] > 90 && curso[j].apellido[k] < 97) || (curso[j].apellido[k] > 122))
							{
								bandera= 0;
							}
							else
							{
								bandera= 1;
							}
							k++;
						}
					}
					k= 0;
					
					
					// Legajo //
					printf ("[%d] Ingrese el legajo: ", j);
					bandera= scanf ("%d", &curso[j].legajo);
					while(bandera != 1)
					{
						system ("cls");
						printf ("ERROR fatal:");
						printf ("\n\n%cLa opci%cn ingresada no es v%clida.\n\n", 07, 162, 160);
						system ("pause");
						system ("cls");
						printf ("[%d] Reingrese el legajo: ", i);
						while ((buffer = getchar()) != '\n' && buffer != EOF);			//Limpiar buffer de entrada.
						bandera= scanf ("%d", &curso[j].legajo);
					}
					
					// 1er parcial Pr�ctico //
					printf ("[%d] Ingrese la nota del primer parcial practico: ", j);
					scanf ("%d", &curso[j].notas[0]);
					while (curso[j].notas[0] < 1 || curso[j].notas[0] > 10)
					{
						system ("cls");
						printf ("ERROR fatal:");
						printf ("\n\n%cLa opci%cn ingresada no es v%clida.\n\n", 07, 162, 160);
						system ("pause");
						system ("cls");
						printf ("[%d] Reingrese la nota del primer parcial practico: ", j);
						scanf ("%d", &curso[j].notas[0]);
					}
					
					// 2do parcial Pr�ctico //
					printf ("[%d] Ingrese la nota del segundo parcial practico: ", j);
					scanf ("%d", &curso[j].notas[1]);
					while (curso[j].notas[1] < 1 || curso[j].notas[1] > 10)
					{
						system ("cls");
						printf ("ERROR fatal:");
						printf ("\n\n%cLa opci%cn ingresada no es v%clida.\n\n", 07, 162, 160);
						system ("pause");
						system ("cls");
						printf ("[%d] Reingrese la nota del segundo parcial practico: ", j);
						scanf ("%d", &curso[j].notas[1]);
					}
					
					// 1er parcial Te�rico //
					printf ("[%d] Ingrese la nota del primer parcial te%crico: ", j, 162);
					scanf ("%d", &curso[j].notas[2]);
					while (curso[j].notas[2] < 1 || curso[j].notas[2] > 10)
					{
						system ("cls");
						printf ("ERROR fatal:");
						printf ("\n\n%cLa opci%cn ingresada no es v%clida.\n\n", 07, 162, 160);
						system ("pause");
						system ("cls");
						printf ("[%d] Reingrese la nota del primer parcial te%crico: ", j, 162);
						scanf ("%d", &curso[j].notas[2]);
					}
					
					// 2do parcial Te�rico //
					printf ("[%d] Ingrese la nota del segundo parcial te%crico: ", j, 162);
					scanf ("%d", &curso[j].notas[3]);
					while (curso[j].notas[3] < 1 || curso[j].notas[3] > 10)
					{
						system ("cls");
						printf ("ERROR fatal:");
						printf ("\n\n%cLa opci%cn ingresada no es v%clida.\n\n", 07, 162, 160);
						system ("pause");
						system ("cls");
						printf ("[%d] Reingrese la nota del segundo parcial te%crico: ", j, 162);
						scanf ("%d", &curso[j].notas[3]);
					}
					
					printf ("\n");
					system ("pause");
				}
				break;
			case Lista_de_alumnos:
				system ("cls");
				printf("Lista de alumnos:");
				printf ("\n\n|Legajo|     |Notas pr%ctico|     |Notas te%crico|     |Nombre y Apellido|     |Estado acad%cmico|\n", 160, 162, 130);
				for (k=1; k<i; k++)
				{
					printf ("\n[%d]           %d - %d               %d - %d          %s, %s", curso[k].legajo, curso[k].notas[0], curso[k].notas[1], curso[k].notas[2], curso[k].notas[3], curso[k].apellido, curso[k].nombre);
					if (curso[k].notas[0] < 4 && curso[k].notas[1] < 4)
					{
						printf (" -> %s", GetString(Libre));
					}
					if (curso[k].notas[0] >= 4 && curso[k].notas[1] >= 4 && (curso[k].notas[0] < 7 || curso[k].notas[1] < 7) && (curso[k].notas[2] < 7 || curso[k].notas[3] < 7))
					{
						printf (" -> %s", GetString(Regular));
					}
					if (curso[k].notas[0] >= 7 && curso[k].notas[1] >= 7 && (curso[k].notas[2] < 7 || curso[k].notas[3] < 7))
					{
						printf (" -> %s", GetString(Promocion_practica));
					}
					if (curso[k].notas[0] >= 4 && curso[k].notas[0] < 7 && curso[k].notas[1] >= 4 && curso[k].notas[1] < 7 && curso[k].notas[2] >= 7 && curso[k].notas[3] >= 7)
					{
						printf (" -> %s", GetString(Promocion_Teorica));
					}
					if (curso[k].notas[0] >= 7 && curso[k].notas[1] >= 7 && curso[k].notas[2] >= 7 && curso[k].notas[3] >= 7)
					{
						printf (" -> %s", GetString(Aprobacion_Directa));
					}
				}
				printf ("\n\n");
				system ("pause");
				break;
			case Finalizar_el_programa:
				system ("cls");
				printf ("Salir:");
				printf ("\n\nPrograma finalizado correctamente.\n\n");
				system ("pause");
				break;
			default:
				system ("cls");
				printf ("ERROR fatal:");
				printf ("\n\n%cLa opci%cn ingresada no es v%clida.\n\n", 07, 162, 160);
				system ("pause");
				break;
			}
			
		} while(opcion != 27);
	}
	
	return 0;
}

char *GetString (int cond)
{
	switch (cond)
	{
	case Libre:
		return "Libre";
		break;
	case Regular:
		return "Regular";
		break;
	case Promocion_practica:
		return "Promocion-practica";
		break;
	case Promocion_Teorica:
		return "Promocion-Teorica";
		break;
	case Aprobacion_Directa:
		return "Aprobacion-Directa";
		break;
	default:
		return "Error";
	}
}
