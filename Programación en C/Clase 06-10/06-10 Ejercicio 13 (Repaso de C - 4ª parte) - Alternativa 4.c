/* 10 de junio ejercicio 13
Desarrolle un programa en lenguaje C que incluya lo siguiente:
1- Definici�n de una estructura adecuada para almacenar datos de un alumno 
(nombre, apellido, legajo, notas, condici�n). La condici�n de los alumnos ser�: 
LIBRE, REGULAR, PROMOCION PRACTICA, PROMOCION TEORICA, APROBACION DIRECTA. Este 
dato no ser� cargado por el usuario, sino que ser� determinado de manera 
autom�tica por el programa. Utilice como gu�a el reglamento de la catedra 
Inform�tica II.
2- Permitir cargar una lista de alumnos con sus datos y las notas 
correspondientes a una determinada asignatura en un arreglo de la estructura 
definida en 1. Debe implementarse algun mecanismo (mediante men�) para permitir 
al usuario cargar los alumnos y detener la carga en cualquier momento. Se podr�n 
cargar como m�ximo 50 alumnos.
3- Deber� ofrecer la posibilidad de modificar los datos de cualquier alumno que 
ya haya sido cargado.
4- Finalizada la carga, el programa deber� ofrecer la opci�n de mostrar en 
pantalla el listado completo de alumnos y sus datos o mostrar un resumen 
indicando el porcentaje de los alumnos en cada condici�n.
5- Utilizar comentarios para explicar el funcionamiento de cada funcion y 
otras partes relevantes del c�digo implementado.
6- Implementar los controles que considere necesario tales como verificar 
valores validos en la carga de datos, permitiendo re introducirlos tras un 
error.*/
/*Actualizaciones:
Programa totalmente diferente con uso de funciones*/

#include <stdio.h>

//Tipos de Datos
typedef enum {F=0, V=1} boolean;

typedef enum {LIBRE=0, REGULAR= 1, PROMO_TEORICA= 2, PROMO_PRACTICA= 3,
	APROBACION_DIRECTA= 5
}cond;

enum opciones {cargar= 1, modificar= 2, mostrar= 3, salir= 4};

typedef struct {
	char nombre [40];
	char apellido [40];
	int legajo;
	int tnotas_teorico [3];
	int pnotas_practico [3];
	cond condicion;
}alumnos;
	
alumnos info2[50];

// Prototipo de Funciones
int Menu_Principal ();
void Cargar_Alumno();
void Modificar_Datos ();
void Mostrar_Cargados ();
char* GetString (cond salida);
boolean controlar ();
void Calc_Condicion (int v);

int i= 0;
int n= 0;

int main(int argc, char *argv[]) {
	printf ("Informatica II - 2R3");
	printf ("\nBienvenido, este programa le permite ingresar alumnos al curso como tambien modificar sus datos");
	printf ("\nNota: Si ingresa otro valor en el menu, se pedira nuevamente hasta hallar coincidencia\n");
	int opcion= 0;
	/*El switch determina que funcion se va a ejecutar, este menu aparece hasta
	que el usuario decida salir del programa.*/
	while (1)
	{
		opcion= Menu_Principal ();
		switch (opcion)
		{
			case cargar:
				Cargar_Alumno ();
				break;
			case modificar:
				Modificar_Datos ();
				break;
			case mostrar:
				Mostrar_Cargados ();
				break;
			case salir:
				return 0;
				break;
			default:
				printf ("\nERROR, opcion erronea");
		}
	}
	
	return 0;
}

//Funciones
int Menu_Principal ()
{
	printf ("\nTiene las Siguientes Opciones:\n1)Cargar Alumnos\n2)Modificar Datos\n3)Mostrar Cargados\n4)Salir del Programa");
	int opt= 0;
	do
	{
		printf ("\nIngrese su opcion: ");
		scanf ("%d", &opt);
	} while(opt < 1 && opt > 4);
	
	return opt;
}

void Cargar_Alumno ()
{
	if (n < 50)
	{
		printf ("\nIngrese los datos del registro #%d", n+1);
		printf ("\nNombre: ");
		scanf ("%s", info2[n].nombre);
		printf ("Apellido: ");
		scanf ("%s", info2[n].apellido);
		/*La funcion controlar devuelve un valor bool, controla que el legajo no 
		este repetido y luego permite el ingreso*/
		do
		{
			printf ("\nLegajo (no puede estar repetido: ");
			scanf (" %d", &info2[n].legajo);
		} while(controlar () == F);
		
		for (i=0; i<3; i++)
		{
			printf ("\nNota Teorica %d: ", i+1);
			scanf (" %d", &info2[n].tnotas_teorico[i]);
		}
		
		for (i= 0; i<3; i++)
		{
			printf ("\nNota Practica %d: ", i+1);
			scanf (" %d", &info2[n].pnotas_practico[i]);
		}
		// Los ciclos for piden las tres notas de cada parte de la materia.
		//Luego Calc Condicion determina la condicion del alumno en base a sus notas
		Calc_Condicion (n);
		n++;
	}
}

void Modificar_Datos ()
{
	/*La funcion muestra los legajos disponibles, se elige uno y se reemplazan 
	los datos en la estructura correcpondiente*/
	/*El procedimiento de reemplazo de datos es el mismo que de la carga de 
	alumnos nuevos*/
	if (n > 0)
	{
		int leg;
		printf ("\nLista de legajos");
		for (i= 0; i<n; i++)
		{
			printf ("\n%d: %d", i+1, info2[i].legajo);
		}
		int e= -1;
		printf ("\nIngrese el legajo a modificar, si no es correcto volvera al menu\n-");
		scanf (" %d", &leg);
		for (i=0; i<n; i++)
		{
			if (leg == info2[i].legajo)
			{
				e= i;
			}
		}
		if (e != -1)
		{
			printf ("\nNombre: ");
			scanf (" %s", info2[e].nombre);
			printf ("\nApellido: ");
			scanf (" %s", info2[e].apellido);
			
			do {
				printf ("\nLegajo (no puede estar repetido): ");
				scanf (" %d", &info2[e].legajo);
			} while(controlar () == F);
			
			for (i= 0; i<3; i++)
			{
				printf ("\nNota Teorica %d: ", i+1);
				scanf (" %d", &info2[e].tnotas_teorico[i]);
			}
			for (i= 0; i<3; i++)
			{
				printf ("\nNota Practica %d: ", i+1);
				scanf (" %d", &info2[e].pnotas_practico[i]);
			}
			Calc_Condicion (e);
		}
		else
		{
			printf ("\nNo hay alumnos cargados\n");
		}
	}
}

void Mostrar_Cargados ()
{
	/*Escribe una linea con la descripcion de cada columna luego coloca en filas
	subsiguientes los datos de los registros*/
	printf ("\nLegajo\tApellido/Nombre\tN.Teor.\tN.Prac.\tCondicion\n");
	for (i=0; i<n; i++)
	{
		printf ("\n%d\t%s %s\t%d %d %d\t%d %d %d\t %s\n", info2[i].legajo, info2[i].apellido, info2[i].nombre, info2[i].tnotas_teorico[0], info2[i].tnotas_teorico[1], info2[i].tnotas_teorico[2], info2[i].pnotas_practico[0], info2[i].pnotas_practico[1], info2[i].pnotas_practico[2], GetString (info2[i].condicion));
			}
}

char *GetString (cond salida)
{
	//Devuelve un string segun el valor del enum 
	/*Para evitar adventencias en el compilador esta el string "error", sin 
	embargo, el dise�o del programa evita llegar a ese punto*/
	switch (salida)
	{
	case 0:
		return "Libre";
	case 1:
		return "Regular";
	case 2:
		return "P.teorica";
	case 3:
		return "P.Practica";
	case 5:
		return "A.Directa";
	}
	return "error";
}

boolean controlar ()
{
	/*Como ya se explico, se recorren los registros y se comprueba que no haya 
	repeticion con el ultimo.*/
	boolean rep= V;
	for (i=0; i<n; i++)
	{
		if (info2[n].legajo == info2[i].legajo)
		{
			rep= F;
			return rep;
		}
	}
	return rep;
}

void Calc_Condicion (int v)
{
	if (info2[v].pnotas_practico[0] < 4 && info2[v].pnotas_practico[1] < 4 && info2[v].pnotas_practico[2] < 4)
	{
		info2[v].condicion= LIBRE;
	}
	
	if (info2[v].pnotas_practico[0] >= 4 && info2[v].pnotas_practico[1] >= 4 && info2[v].pnotas_practico[2] >= 4 && (info2[v].pnotas_practico[0] < 7 || info2[v].pnotas_practico[1] < 7 || info2[v].pnotas_practico[2] < 7) && (info2[v].tnotas_teorico [0] < 7 || info2[v].tnotas_teorico [1] < 7 || info2[v].tnotas_teorico [2] < 7))
	{
		info2[v].condicion= REGULAR;
	}
	if (info2[v].pnotas_practico[0] >= 7 && info2[v].pnotas_practico[1] >= 7 && info2[v].pnotas_practico[2] >= 7 && (info2[v].tnotas_teorico [0] < 7 || info2[v].tnotas_teorico [1] < 7 || info2[v].tnotas_teorico [2] < 7))
	{
		info2[v].condicion= PROMO_PRACTICA;
	}
	if (info2[v].pnotas_practico[0] >= 4 && info2[v].pnotas_practico[0] < 7 && info2[v].pnotas_practico[1] >= 4 && info2[v].pnotas_practico[1] < 7 && info2[v].pnotas_practico[2] >= 4 && info2[v].pnotas_practico[2] < 7 && info2[v].tnotas_teorico [0] >= 7 && info2[v].tnotas_teorico [1] >= 7 && info2[v].tnotas_teorico [2] >= 7)
	{
		info2[v].condicion= PROMO_TEORICA;
	}
	if (info2[v].pnotas_practico[0] >= 7 && info2[v].pnotas_practico[1] >= 7 && info2[v].pnotas_practico[2] >= 7 && info2[v].tnotas_teorico [0] >= 7 && info2[v].tnotas_teorico [1] >= 7 && info2[v].tnotas_teorico [2] >= 7)
	{
		info2[v].condicion= APROBACION_DIRECTA;
	}
}
