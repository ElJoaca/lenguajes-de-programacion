/* 17 de junio ejemplo 5
Escriba un programa en C que lea un archivo de texto, cuente cuantas palabras y
oraciones contiene y verifique que cada oraci�n comience con letra may�scula, en
caso contrario que la corrija.*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main(int argc, char *argv[]) {
	FILE *archivo;
	FILE *archivo_nuevo;
	int palabras= 0;
	int oraciones= 0;
	int control_de_palabras= 0;
	int control_de_oraciones= 0;
	char c;
	
	if((archivo= fopen("06-17 Ejemplo 5 del apunte - Archivos (original).txt", "r")) != NULL)
	{
		if((archivo_nuevo= fopen("06-17 Ejemplo 5 del apunte - Archivos (modificado).txt", "w")) != NULL)
		{
			c= fgetc(archivo);
			if (islower(c))														//Caso particular: el archivo comienza con una min�scula.
			{
				fputc(toupper(c), archivo_nuevo);
			}
			do 																	//Se ejecuta el do while mientras no se llegue al EOF.
			{
				c= fgetc (archivo);
				
				if (feof(archivo) == 0)											//Debo verificar que el caracter a escribir en "archivo_nuevo" no sea el caracter nulo.					
				{
					fputc(c, archivo_nuevo);
				}
				
				if (c == ' ')													//Si leo un espacio termina una palabra.
				{
					palabras++;
					control_de_palabras= 0;
				}
				
				if (c == '.')													//Si leo un punto termina una oraci�n.
				{
					oraciones++;
					control_de_oraciones= 0;
					
					c= fgetc(archivo);											//El siguiente caracter debe ser un espacio
					if (c == ' ')
					{
						fputc(c, archivo_nuevo);
						palabras++;												//Si leo un espacio, termina una palabra.
						control_de_palabras= 1;
						control_de_oraciones= 1;
						
						c= fgetc(archivo);										//El siguiente caracter debe ser may�scula.
						if (islower(c))											//islower me dice si un caracter es min�scula o may�scula.
						{
							fputc(toupper(c), archivo_nuevo);					//Reemplazo por el char en may�scula.
						}
						else
						{
							fputc(c, archivo_nuevo);
						}
					}
				}
				
				if (feof(archivo) != 0 && control_de_palabras != 0)				//Caso particular: el archivo no termina en espacio (" ") y no me cuenta la �ltima palabra.
				{
					palabras++;
				}
				
				if (feof(archivo) != 0 && control_de_oraciones != 0)			//Caso particular: el archivo no termina en punto (".") y no me cuenta la �ltima palabra.
				{
					oraciones++;
				}
				
			} while(feof(archivo) == 0);

			printf ("Cantidad de palabras: %d", palabras);
			printf ("\n\nCantidad de oraciones: %d", oraciones);
			
			fclose(archivo_nuevo);
		}
		else
		{
			printf("\nNo se pudo crear el archivo!");
		}
		fclose(archivo);
	}
	else
	{
		printf("\nNo se pudo crear el archivo!");
	}
	
	return 0;
}
