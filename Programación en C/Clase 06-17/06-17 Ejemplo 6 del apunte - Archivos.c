/* 17 de junio ejemplo 6
Escriba un programa en C que reciba por consola la informaci�n de numero de
legajo y 3 calificaciones (0 a 10) de N alumnos y almacene esta informaci�n en
un archivo de texto "notas.txt". En cada l�nea del archivo se guardar� la
informaci�n de cada alumno en el siguiente orden:
	Legajo nota1 nota2 nota3*/

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
	FILE *archivo;
	int n;
	int legajo;
	int nota;
	int i, j;
	
	printf("Ingrese el numero de alumnos: ");
	scanf("%d", &n);
	if ((archivo= fopen("06-17 Ejemplo 6 del apunte - Archivos.txt", "w")) != NULL)	//Creo el archivo.
	{
		for (i=0; i<n; i++)
		{
			printf("\nIngrese el legajo del alumno %d: ", i);
			scanf("%d", &legajo);
			fprintf(archivo, "%d", legajo);
			for (j=0; j<3; j++)
			{
				printf("\nIngrese nota %d: ", j);
				scanf("%d", &nota);
				if (nota < 0 || nota > 10)
				{
					do
					{
						printf("\nIngrese nota %d: ", j);
						scanf("%d", &nota);
					} while(nota < 0 || nota > 10);
				}
				fprintf(archivo, " %d", nota);
			}
			fprintf(archivo, "\n");												//Para que me quede 1 alumno por cada rengl�n.
		}
		fclose(archivo);
	}
	else
	{
		printf("\nNo se pudo crear el archivo!");
	}
	
	return 0;
}
