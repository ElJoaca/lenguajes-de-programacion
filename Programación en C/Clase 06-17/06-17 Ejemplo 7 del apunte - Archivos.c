/* 17 de junio ejemplo 7
Escriba un programa en C que abra en modo lectura el archivo creado por el
programa anterior y muestre en pantalla el legajo y promedio de cada alumno as�
como el promedio general de todos los alumnos*/

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
	FILE *archivo;
	int legajo;
	int nota= 0;
	int n= 0;
	int i, j;
	float curso= 0;
	float promedio= 0;
	char c;

	if ((archivo= fopen("06-17 Ejemplo 6 del apunte - Archivos.txt", "r")) != NULL)
	{
		while (feof(archivo) == 0)												//Veo cuantos alumnos hab�a en el archivo. Un "truco" es colocar en un primer rengl�n solamente la cantidad de alumnos que hay, as� me evito esto.
		{
			c= fgetc (archivo);
			if (c == '\n')
			{
				n++;
			}
		}
		
		rewind(archivo);
		printf("Numero de lineas: %d\n", n);
		if (n>0)																//Si hay m�s de 0 alumnos.
		{
			printf("Legajo\tPromedio:\n");
			for (i=0; i<n; i++)
			{
				promedio= 0;
				fscanf(archivo, "%d", &legajo);
				printf("%d: ", legajo);
				for (j=0; j<3; j++)
				{
					fscanf(archivo, "%d", &nota);
					promedio += nota;
				}
				printf("\t%.2f\n", promedio/3.0f);								//Divide por 3.0f para que no se me pierda la parte decimal de la divisi�n.
				curso += promedio/3.0f;
			}
			printf("Promedio del curso: %.2f\n", curso/(float)n);
		}
		fclose(archivo);
	}
	else
	{
		printf("\nNo se pudo crear el archivo!");
	}
	return 0;
}
