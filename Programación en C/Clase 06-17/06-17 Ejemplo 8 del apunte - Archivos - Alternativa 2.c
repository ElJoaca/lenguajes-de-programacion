/* 17 de junio ejemplo 8
Modifique el programa del ejercicio 6 para solicitar además el nombre del alumno
y almacena la información de cada alumno en una variable de tipo struct. Luego
almacene la información de todos los alumnos directamente en un archivo de tipo
binario.*/

#include <stdio.h>
#include <stdlib.h>

typedef struct {
	int legajo;
	int notas[3];
	char nombre[10];
} Alumno;

int main(int argc, char *argv[]) {
	FILE *archivo;
	int n;
	int legajo;
	int nota;
	int nb;
	int i, j;
	Alumno a;
	
	printf("Ingrese el numero de alumnos: ");
	scanf("%d", &n);
	if ((archivo= fopen("06-17 Ejemplo 8 del apunte - Archivos - Alternativa 2.txt", "wb")) != NULL)	//Creo el archivo:
	{
		for (i=0; i<n; i++)
		{
			printf("\nIngrese el nombre del alumno %d: ", i);
			scanf("%s", &a.nombre);
			printf("\nIngrese el legajo del alumno %d: ", i);
			scanf("%d", &a.legajo);
			for (j=0; j<3; j++)
			{
				printf("\nIngrese nota %d: ", j);
				scanf("%d", &a.notas[j]);
			}
			nb= fwrite(&a,sizeof(a),1,archivo);
		}
		fclose(archivo);
	}
	else
	{
		printf("\nNo se pudo crear el archivo!");
	}
	return 0;
}
