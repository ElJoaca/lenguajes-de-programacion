/* 17 de junio ejemplo 9
Escriba un programa en C que abra en modo lectura el archivo creado por el 
programa anterior y muestre en pantalla el legajo, nombre y promedio de cada 
alumno as� como el promedio general de todos los alumnos.*/

#include <stdio.h>
#include <stdlib.h>

typedef struct {
	int legajo;
	int notas[3];
	char nombre[10];
} Alumno;

int main(int argc, char *argv[]) {
	FILE *archivo;
	int n= 0;
	int i, j;
	float curso= 0;
	float promedio= 0;
	Alumno a;

	if ((archivo= fopen("06-17 Ejemplo 8 del apunte - Archivos - Alternativa 2.txt", "rb")) != NULL)
	{
		while (fread(&a, sizeof(Alumno), 1, archivo) == 1)						//Veo cuantos alumnos hab�a en el archivo. Un "truco" es colocar en un primer rengl�n solamente la cantidad de alumnos que hay, as� me evito esto.
		{
			n++;
		}

		rewind(archivo);
		printf("Numero de lineas: %d\n", n);
		if (n>0)																//Si hay m�s de 0 alumnos.
		{
			printf("|Legajo|     |Promedio|     |Nombre|\n");
			for (i=0; i<n; i++)
			{
				promedio= 0;
				fread(&a, sizeof(a),1, archivo);
				printf("[%d]         ", a.legajo);
				for (j=0; j<3; j++)
				{
					promedio += a.notas[j];
				}
				printf("%.2f        ", promedio/3.0f);							//Divide por 3.0f para que no se me pierda la parte decimal de la divisi�n.
				curso += promedio/3.0f;
				printf("%s\n", a.nombre);
			}
			printf("Promedio del curso: %.2f\n", curso/(float)n);
		}
		fclose(archivo);
	}
	else
	{
		printf("\nNo se pudo crear el archivo!");
	}
	return 0;
}
