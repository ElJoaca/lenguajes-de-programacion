/* 24 de junio Ejemplo 11.30
A�adamos ahora otro componente, cuyo valor es blanco, entre tojo y verde. Para 
hacer esto simplemente cambiamos los punteros, como se ilustra en la Figura 
11.3(b). An�logamente, si elegimos borrar el elemento cuyo valor es verde 
sencillamente cambiamos el puntero asociado con el segundo componente, como se
muestra en la Figura 11.3(c).*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct nodo {																	//Defino estructura autoreferenciada.
	char color[10];
	struct nodo *ptr;
};

struct nodo * CrearNodo (char *dato);											//Crea el nodo, recibe como dato el vector de char y devuelve el puntero al nodo creado.

void MostrarLista (struct nodo *ptr);

int main(int argc, char *argv[]) {
	struct nodo *inicio;														//Puntero para el inicio de la lista.
	struct nodo *pnodo;															//Puntero a nodo para moverme dentro la la lista.
	
	pnodo= CrearNodo("Rojo");													//Crea el nodo, va a reservar la memoria para un nuevo nodo y le almacena la informacion rojo. Me va a devolver el puntero a ese nodo que cre�.
	inicio= pnodo;
	
	pnodo -> ptr= CrearNodo("Verde");											//La direcci�n del nodo verde la copio en el puntero de mi nodo actual pnodo (en este caso el rojo). Se crea el enlace entre el nodo rojo y el nodo verde.
	pnodo= pnodo -> ptr;														//Muevo pnodo al nodo que acabo de crear que hab�a guardado en ptr.
	
	pnodo -> ptr= CrearNodo("Azul");											//El puntero del nodo azul lo guardo en el puntero del nodo anterior.
	pnodo= pnodo -> ptr;														//Actualizo el valor de pnodo al puntero que acabo de crear que est� ubicado en pnodo -> ptr.
	
	MostrarLista(inicio);
	
	struct nodo *nuevo;															// Insertar nodo blanco despues del rojo.
	
	nuevo= CrearNodo("blanco");
	pnodo= inicio;
	do
	{
		if (strcmp (pnodo -> color, "Rojo") == 0)								//Como estoy con cadena de caracteres, voy a usar funcion de string.h strcmp que compara la cadena color con Rojo.
		{
			nuevo -> ptr= pnodo -> ptr;											//Al nodo nuevo (que es blanco), a su puntero ptr le guardo la direcci�n del nodo verde. Rojo est� en pnodo. En el puntero de blanco coloco la direccion de verde.
			pnodo -> ptr= nuevo;												//A rojo lo hago apuntar al nodo nuevo que acabo de crear.
		}
		pnodo= pnodo -> ptr;
	} while(pnodo != NULL);
	
	MostrarLista(inicio);
	
	return 0;
}

struct nodo * CrearNodo(char *dato)
{
	struct nodo *pnodo;
	
	pnodo= (struct nodo*) malloc (sizeof(struct nodo));							//Primero reservo memoria
	if (pnodo != NULL)															//Verifico que esa memoria no sea nula.
	{
		strcpy(pnodo -> color, dato);											//strcpy: copia una cadena de caracteres. Guardo la informaci�n.
		pnodo -> ptr= NULL;														//En el puntero de ese nodo le pongo NULL.
	}																			//pnodo es un puntero a una estructura, es por eso que uso las flechas.
	return pnodo;																
}																		

void MostrarLista (struct nodo *pnodo)											//pnodo es una variable local. Esta l�nea es equivalente a escribir: struct nodo *nodo= pnodo;
{																				//Donde se crea la variable local "*pnodo" y le guardo adentro lo que reciba como argumento de entrada de "pnodo".
	if (pnodo != NULL)															//Recibe como argumento de entrada un puntero a nodo struct nodo *. Y por otro lado crea la variable local pnodo.
	{
		do
		{
			printf("%s, ", pnodo -> color);
			pnodo= pnodo -> ptr;
		} while(pnodo != NULL);
		printf("\n");
	}
}
