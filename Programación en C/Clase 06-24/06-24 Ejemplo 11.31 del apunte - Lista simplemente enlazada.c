/* 24 de junio ejemplo 11.31
En la figura 11.4 vemos una lista lineal enlazada similar a la mostrada en la 
Figura 11.3(a). Sin ambargo, ahora hay dos punteros asociados con cada
componente: un puntero al siguiente y un puntero al anterior. Este doble
conjunto de punteros permite recorrer la lista en cualquier direcci�n, de
principio a fin, o del fin al principio*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct nodo {																	//Defino estructura autoreferenciada.
	char color[10];
	struct nodo *panterior;
	struct nodo *psiguiente;
};

struct nodo * CrearNodo (char *dato);											//Crea el nodo, recibe como dato el vector de char y devuelve el puntero al nodo creado.

void MostrarLista (struct nodo *ptr);

int main(int argc, char *argv[]) {
	struct nodo *inicio;														//Puntero para el inicio de la lista.
	struct nodo *pnodo;															//Puntero a nodo para moverme dentro la la lista.
	
	pnodo= CrearNodo("Rojo");
	inicio= pnodo;
	
	pnodo -> psiguiente= CrearNodo("Verde");									//Cuando creo el segundo nodo verde, que ahora lo voy a guardar/enlazar en psiguiente de rojo (el nodo anterior). Rojo apunta en siguiente a verde (en psiguiente tengo a verde).
	(pnodo -> psiguiente) -> panterior= pnodo;									//Actualizo, pnodo siempre apunta al nodo que acabo de crear. // Pnodo -> psiguiente es verde. Verde -> panterior va a ser pnodo. // Ahora quiero que verde apunte a rojo. // psiguiente (que es verde) quiero que apunte a rojo que lo tengo guardado en pnodo. // (pnodo -> psiguiente) todo eso es verde.
	pnodo= pnodo -> psiguiente;													//Ahora a verde que ya lo tengo en pnodo, en su nodo panterior, verde en panterior tiene a rojo. // Recien ahora estoy en condiciones de actualizar mi puntero actual a verde.
	
	pnodo -> psiguiente= CrearNodo("Azul");										//El puntero del nodo azul lo guardo en el puntero del nodo anterior.
	(pnodo -> psiguiente) -> panterior= pnodo;									//Voy a etar uniendo azul a verde.
	pnodo= pnodo -> psiguiente;													//Actualizo el valor de pnodo al puntero que acabo de crear que est� ubicado en pnodo -> ptr.
	
	MostrarLista(inicio);
	
	struct nodo * nuevo;
	
	nuevo= CrearNodo("blanco");
	pnodo= inicio;
	do
	{
		if (strcmp (pnodo -> color, "Rojo") == 0)								//Como estoy con cadena de caracteres, voy a usar funcion de string.h strcmp que compara la cadena color con Rojo.
		{
			nuevo -> psiguiente= pnodo -> psiguiente;
			pnodo -> psiguiente= nuevo;
			
			nuevo -> panterior= pnodo;
			(nuevo -> psiguiente) -> panterior= nuevo;
		}
		pnodo= pnodo -> psiguiente;
	} while(pnodo != NULL);
	
	MostrarLista(inicio);
	
	pnodo= inicio;
	do
	{
		if (strcmp (pnodo -> color, "Verde") == 0)								//En este punto pnodo es el nodo verde
		{
			(pnodo -> psiguiente) -> panterior= pnodo -> panterior;
			(pnodo -> panterior) -> psiguiente= pnodo -> psiguiente;
			strcpy(pnodo -> color, "XXXXXXXXXX") ;
			free(pnodo);														//Eliminar verde liberando la memoria. Me libera pnodo que contiene referencia al nodo verde.
		}																		//Tambi�n puedo borrar los datos de la memoria usando: strcpy(pnodo -> color, "XXXXXXXXXX");
		pnodo= pnodo -> psiguiente;
	} while(pnodo != NULL);
	
	MostrarLista(inicio);
	
	return 0;
}

struct nodo * CrearNodo(char *dato)
{
	struct nodo *pnodo;
	
	pnodo= (struct nodo*) malloc (sizeof(struct nodo));							//Primero reservo memoria
	if (pnodo != NULL)															//Verifico que esa memoria no sea nula.
	{
		strcpy(pnodo -> color, dato);											//strcpy: copia una cadena de caracteres. Guardo la informaci�n.
		pnodo -> psiguiente= NULL;												//En el puntero de ese nodo le pongo NULL.
		pnodo -> panterior= NULL;												//pnodo es un puntero a una estructura, es por eso que uso las flechas.
	}																			//Inicializo los punteros que haya a 0.
	return pnodo;
}																		

void MostrarLista (struct nodo *pnodo)											//pnodo es una variable local. Esta l�nea es equivalente a escribir: struct nodo *nodo= pnodo;
{																				//Donde se crea la variable local "*pnodo" y le guardo adentro lo que reciba como argumento de entrada de "pnodo".
	if (pnodo != NULL)															//Recibe como argumento de entrada un puntero a nodo struct nodo *. Y por otro lado crea la variable local pnodo.
	{
		do
		{
			printf("%s, ", pnodo -> color);
			pnodo= pnodo -> psiguiente;
		} while(pnodo != NULL);
		printf("\n");
	}
}
