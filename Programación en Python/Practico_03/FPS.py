import sys
import cv2

if (len (sys.argv) > 1):
  filename= sys.argv[1]
else:
  print ('Pasar el nombre del video como argumento')
  sys.exit(0)

cap=cv2.VideoCapture(filename)
fps= cap.get(cv2.CAP_PROP_FPS)
print()
print ('ESte video tiene {0:.1f} fps.'.format(fps))
t=int((1/fps)*1000)
print ('Valor de t en milisegundos: {0}.'.format(t))

while (cap. isOpened()):
    ret, frame= cap.read()
    if ret:
        gray= cv2. cvtColor(frame, cv2.COLOR_BGR2GRAY)
        cv2.imshow('frame', gray)
    if ((cv2.waitKey(t) & 0xFF) == ord('q')):
        break

print('Programa finalizado.')
print()
cap.release()
cv2.destroyAllWindows()
