import cv2

cap= cv2.VideoCapture(0)
fourcc= cv2.VideoWriter_fourcc('X', 'V', 'I', 'D')
w= cam.get(cv2.CV_CAP_PROP_FRAME_WIDTH)
h= cam.get(cv2.CV_CAP_PROP_FRAME_HEIGHT)
out= cv2.VideoWriter('output.avi', fourcc, 20.0, (w, h))
print()
print ('Resolucion del video: {0}x{1}.'.format(w, h))

while(cap.isOpened()):
  ret, frame= cap.read()
  if ret is True:
    out.write(frame)
    cv2.imshow('frame', frame)
    if cv2.waitKey(1) & 0xFF== ord ('q'):
      break
    else:
      break

print('Programa finalizado.')
print()
cap.release()
out.release()
cv2.destroyAllWindows()
