import cv2
import numpy as np

black= (0,0,0)                                                   # Tupla que tiene al mínimo los 3 canales. Negro puro.
drawing= False
mode= True
xybutton_down= -1, -1
enable= True

def dibuja (event, x, y, flags, param):                          # event: ¿qué evento ocurrió?. (x, y), en que coordenada ocurrió.
  global drawing, mode, xybutton_down, enable, img               # Para decirle a python que drawing, mode y xybutton_down son las mismas variables que están más arriba y que no me cree otra.
  if (event == cv2.EVENT_LBUTTONDOWN) & (enable == True):        # Click izquierdo presionado.
    drawing= True
    xybutton_down= x, y
  elif (event == cv2.EVENT_MOUSEMOVE) & (enable == True):        # El mouse de mueve de ubicación.
    if drawing is True:
      img= cv2.imread('Hoja.png')                                # Restauro la imagen mientras dibujo el rectángulo.
      if mode is True:
        cv2.rectangle (img, xybutton_down, (x, y), black, 1)     # Dibujo en img, desde xybutton_down hasta (x,y) de color negro y grosor 1.
  elif (event == cv2.EVENT_LBUTTONUP) & (enable == True):        # Click izquierdo levantado.
    if (xybutton_down[1] < y) & (xybutton_down[0] < x):
        img= img[xybutton_down[1]:y,xybutton_down[0]:x]
    elif (xybutton_down[1] < y) & (xybutton_down[0] > x):
        img= img[xybutton_down[1]:y,x:xybutton_down[0]]
    elif (xybutton_down[1] > y) & (xybutton_down[0] < x):
        img= img[y:xybutton_down[1],xybutton_down[0]:x]
    else:
        img= img[y:xybutton_down[1],x:xybutton_down[0]]
    drawing= False
    enable= False                                                # Para darle fin al rectangulo que se está dibujando.

img= cv2.imread('Hoja.png')                                      # Imagen/matriz de zeros (negra) de 512x512 con 3 canales y cada elemento del tipo que quiera (int, float, etc).
cv2.namedWindow('image')                                         # Crea una ventana de nombre: image.
cv2.setMouseCallback('image', dibuja)                            # Asocio la ventana image con la función dibuja (en espera de ser llamada). Cuando el mouse hace algo SOBRE LA VENTANA llama a la función dibuja.

while (1):
    cv2.imshow('image', img)                                     # Muestra img en la ventana image creada anteriormente.
    k= cv2.waitKey(1) & 0xFF
    if k == ord ('g'):                                           # Guarda el recorte al apretar la tecla g.
        cv2.imwrite('Recorte.png', img)
        print ('Imagen guardada.')
    elif (k == ord ('r')) & (enable == False):                   # Restaura la imagen al apretar la tecla r.
        img= cv2.imread('Hoja.png')
        enable= True
        print ('Imagen restaurada.')
    elif k == ord ('q'):                                         # Finaliza el programa al apretar la tecla q.
        print ('Programa finalizado.')
        break
cv2.destroyAllWindows()
