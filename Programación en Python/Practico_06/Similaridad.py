import cv2
import numpy as np

black= (0,0,0)                                                   # Tupla que tiene al mínimo los 3 canales. Negro puro.
drawing= False
mode= True
xybutton_down= -1, -1
enable= 0

def dibuja (event, x, y, flags, param):                          # event: ¿qué evento ocurrió?. (x, y), en que coordenada ocurrió.
  global drawing, mode, xybutton_down, enable, img               # Para decirle a python que drawing, mode y xybutton_down son las mismas variables que están más arriba y que no me cree otra.
  if (event == cv2.EVENT_LBUTTONDOWN) & (enable == 0):           # Click izquierdo presionado.
    drawing= True
    xybutton_down= x, y
  elif (event == cv2.EVENT_MOUSEMOVE) & (enable == 0):           # El mouse de mueve de ubicación.
    if drawing is True:
      img= cv2.imread('Hoja.png')                                # Restauro la imagen mientras dibujo el rectángulo.
      if mode is True:
        cv2.rectangle (img, xybutton_down, (x, y), black, 1)     # Dibujo en img, desde xybutton_down hasta (x,y) de color negro y grosor 1.
  elif (event == cv2.EVENT_LBUTTONUP) & (enable == 0):           # Click izquierdo levantado.
    if (xybutton_down[1] < y) & (xybutton_down[0] < x):
        img= img[xybutton_down[1]:y,xybutton_down[0]:x]
    elif (xybutton_down[1] < y) & (xybutton_down[0] > x):
        img= img[xybutton_down[1]:y,x:xybutton_down[0]]
    elif (xybutton_down[1] > y) & (xybutton_down[0] < x):
        img= img[y:xybutton_down[1],xybutton_down[0]:x]
    else:
        img= img[y:xybutton_down[1],x:xybutton_down[0]]
    drawing= False
    enable= 1                                                   # Para darle fin al rectangulo que se está dibujando.

def translate (image, x, y):                                    # x= desplazamiento horizontal; y= desplazamiento vertical
    (h, w)= (image.shape[0], image.shape[1])
    M= np.float32 ([[1, 0, x],
                    [0, 1, y]])
    shifted= cv2.warpAffine(img, M, (w, h))
    return shifted

def rotate (image, angle, center, scale):
    (h, w)= (image.shape[0], image.shape[1])
    if center is None:
        center= (w/2, h/2)
    M= cv2.getRotationMatrix2D(center, angle, scale)
    rotated= cv2.warpAffine(image, M, (w,h))
    return rotated

img= cv2.imread('Hoja.png')                                      # Imagen/matriz de zeros (negra) de 512x512 con 3 canales y cada elemento del tipo que quiera (int, float, etc).
cv2.namedWindow('image')                                         # Crea una ventana de nombre: image.
cv2.setMouseCallback('image', dibuja)                            # Asocio la ventana image con la función dibuja (en espera de ser llamada). Cuando el mouse hace algo SOBRE LA VENTANA llama a la función dibuja.

while (1):
    cv2.imshow('image', img)                                     # Muestra img en la ventana image creada anteriormente.
    k= cv2.waitKey(1) & 0xFF
    if (k == ord ('s')) & (enable == 1):                         # Es obligatorio usar paréntesis para los 2 parámetros que compara "&"
        x= int(input('Ingrese el desplazamiento horizontal: '))
        y= int(input('Ingrese el desplazamiento vertical: '))
        a= int(input('Ingrese el angulo de rotacion: '))
        s= float(input('Ingrese el factor de escala (ingrese 1 para no escalar): '))
        img= translate(img, x, y)
        img= rotate(img, a, None, s)
        if (x==0) & (y==0) & (a==0) & (s==1):
            print ('Imagen guardada.')
        elif ((x!=0) | (y!=0)) & (a==0) & (s==1):
            print ('Guandando imagen trasladada')
        elif (x==0) & (y==0) & (a!=0) & (s==1):
            print ('Guandando imagen rotada')
        elif (x==0) & (y==0) & (a==0) & (s!=1):
            print ('Guandando imagen escalada')
        elif ((x!=0) | (y!=0)) & (a!=0) & (s==1):
            print ('Guardando imagen trasladada y rotada')
        elif ((x!=0) | (y!=0)) & (a==0) & (s!=1):
            print ('Guardando imagen trasladada y escalada')
        elif (x==0) & (y==0) & (a!=0) & (s!=1):
            print ('Guardando imagen rotada y escalada')
        else:
            print ('Guardando imagen trasladada, rotada y escalada')
        cv2.imwrite('Recorte.png', img)
        enable= 2
    elif k == ord ('g'):                                         # Guarda el recorte al apretar la tecla g.
        cv2.imwrite('Recorte.png', img)
        print ('Imagen guardada.')
    elif (k == ord ('r')) & (enable >= 1):                       # Restaura la imagen al apretar la tecla r.
        img= cv2.imread('Hoja.png')
        enable= 0
        print ('Imagen restaurada.')
    elif k == ord ('q'):                                         # Finaliza el programa al apretar la tecla q.
        print ('Programa finalizado.')
        break
cv2.destroyAllWindows()
