import cv2
import numpy as np

black= (0,0,0)                                                                  # Tupla que tiene al mínimo los 3 canales. Negro puro.
red= (0,0,255)                                                                  # Tupla que tiene al máximo el último canal. Rojo puro.
drawing= False
mode= True
xybutton_down= -1, -1
enable= 0
ciclo= 0
x1=0
x2=0
x3=0
y1=0
y2=0
y3=0

def dibuja (event, x, y, flags, param):                                         # event: ¿qué evento ocurrió?. (x, y), en que coordenada ocurrió.
  global drawing, mode, xybutton_down, enable, img1, img_cpy                             # Para decirle a python que drawing, mode y xybutton_down son las mismas variables que están más arriba y que no me cree otra.
  if (event == cv2.EVENT_LBUTTONDOWN) & (enable == 0):                          # Click izquierdo presionado.
    drawing= True
    xybutton_down= x, y
  elif (event == cv2.EVENT_MOUSEMOVE) & (enable == 0):                          # El mouse de mueve de ubicación.
    if drawing is True:
      img1= cv2.imread('shelley duvall.jpg')                                    # Restauro la imagen mientras dibujo el rectángulo.
      if mode is True:
        cv2.rectangle (img1, xybutton_down, (x, y), black, 1)                   # Dibujo en img1, desde xybutton_down hasta (x,y) de color negro y grosor 1.
  elif (event == cv2.EVENT_LBUTTONUP) & (enable == 0):                          # Click izquierdo levantado.
    if (xybutton_down[1] < y) & (xybutton_down[0] < x):
        img1= img1[xybutton_down[1]:y,xybutton_down[0]:x]
        img_cpy= img_cpy[xybutton_down[1]:y,xybutton_down[0]:x]
    elif (xybutton_down[1] < y) & (xybutton_down[0] > x):
        img1= img1[xybutton_down[1]:y,x:xybutton_down[0]]
        img_cpy= img_cpy[xybutton_down[1]:y,x:xybutton_down[0]]
    elif (xybutton_down[1] > y) & (xybutton_down[0] < x):
        img1= img1[y:xybutton_down[1],xybutton_down[0]:x]
        img_cpy= img_cpy[y:xybutton_down[1],xybutton_down[0]:x]
    else:
        img1= img1[y:xybutton_down[1],x:xybutton_down[0]]
        img_cpy= img_cpy[y:xybutton_down[1],x:xybutton_down[0]]
    drawing= False
    enable= 1                                                                   # Para darle fin al rectangulo que se está dibujando.

def trans_afin (event, x, y, flags, param):
    global img1, img2, img_cpy, ciclo, x1, y1, x2, y2, x3, y3
    (h1, w1)= (img1.shape[0], img1.shape[1])
    (h2, w2)= (img2.shape[0], img2.shape[1])
    if (event == cv2.EVENT_LBUTTONDOWN) & (ciclo == 2):
        cv2.circle(img1, (x, y), 5, red, -1)
        img1=img_cpy
        x3= x
        y3= y
        pts1= np.float32 ([[0, 0], [w2, 0], [0, h2]])
        pts2= np.float32 ([[x1, y1], [x2, y2], [x3, y3]])
        M= cv2.getAffineTransform(pts1, pts2)                                   # El orden de los puntos es importante
        img2= cv2.warpAffine (img2, M, (w1, h1))                                # La imagen 2 es la que sufre la transformación
        #********** Inserición de imagen en otra **********#
        roi = img1[0:h1, 0:w1 ]                                                 # Coordenadas donde se va a insertar la imagen 2 (Sobre toda la imagen 1)
        img2gray = cv2.cvtColor(img2,cv2.COLOR_BGR2GRAY)                        # Imagen 2 en escala de grises

        fila=0                                                                  # Paso de negro a blanco los alrededores de la imgagen 2
        col=0
        for i in img2gray:
        	for j in i:
        		if j == 0:
        			img2gray[fila][col]=255
        		col=col+1
        	fila=fila+1
        	col=0

        ret, mask = cv2.threshold(img2gray, 254, 255, cv2.THRESH_BINARY)        # Guardo en mask (mascara) la imagen 2 binarizada.
        mask_inv = cv2.bitwise_not(mask)                                        # Guardo en mask_inv (mascara invertida) la imagen 2 binarizada invertida.
        img1_bg = cv2.bitwise_and(roi,roi,mask = mask)                          # Reemplaza la zona blanca de la máscara con la zona correspondiente de la imagen 1.
        img2_fg = cv2.bitwise_and(img2,img2,mask = mask_inv)                    # Reemplaza la zona blanca de la máscara invertida con la zona correspondiente de la imagen 2.
        img2 = cv2.add(img1_bg,img2_fg)                                         # Guardo el resultado final en imagen 2 y muestro el resultado
        cv2.imshow('image2', img2)
        #********** Inserición de imagen en otra **********#.
        cv2.imwrite('Recorte.png', img2)
        print ('Imagen guardada.')
    if (event == cv2.EVENT_LBUTTONDOWN) & (ciclo == 1):
        cv2.circle(img1, (x, y), 5, red, -1)
        x2= x
        y2= y
        ciclo=ciclo+1
    if (event == cv2.EVENT_LBUTTONDOWN) & (ciclo == 0):
        cv2.circle(img1, (x, y), 5, red, -1)
        x1= x
        y1= y
        ciclo=ciclo+1


img1= cv2.imread('shelley duvall.jpg')                                          # Imagen/matriz de zeros (negra) de 512x512 con 3 canales y cada elemento del tipo que quiera (int, float, etc).
img2= cv2.imread('Jack Nicholson')
img_cpy= cv2.imread('shelley duvall.jpg')
cv2.namedWindow('image1')                                                       # Crea una ventana de nombre: image.
cv2.setMouseCallback('image1', dibuja)                                          # Asocio la ventana image con la función dibuja (en espera de ser llamada). Cuando el mouse hace algo SOBRE LA VENTANA llama a la función dibuja.

while (1):
    cv2.imshow('image1', img1)                                                  # Muestra img1 en la ventana image creada anteriormente.
    k= cv2.waitKey(1) & 0xFF
    if (k == ord ('a')) & (enable == 1):                                        # Es obligatorio usar paréntesis para los 2 parámetros que compara "&"
        print ('Seleccione 3 puntos.')
        cv2.setMouseCallback('image1', trans_afin)
        enable= 2
    elif k == ord ('g'):                                                        # Guarda el recorte al apretar la tecla g.
        cv2.imwrite('Recorte.png', img1)
        print ('Imagen guardada.')
    elif (k == ord ('r')) & (enable >= 1):                                      # Restaura la imagen al apretar la tecla r.
        img1= cv2.imread('shelley duvall.jpg')
        enable= 0
        ciclo= 0
        cv2.destroyAllWindows()
        print ('Imagen restaurada.')
    elif k == ord ('q'):                                                        # Finaliza el programa al apretar la tecla q.
        print ('Programa finalizado.')
        break
cv2.destroyAllWindows()
