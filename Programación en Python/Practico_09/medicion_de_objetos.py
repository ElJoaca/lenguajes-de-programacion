import math
import cv2
import numpy as np

black= (0,0,0)                                                                  # Tupla que tiene al mínimo los 3 canales. Negro puro.
red= (0,0,255)                                                                  # Tupla que tiene al máximo el último canal. Rojo puro.
drawing= False
mode= True
xybutton_down= -1, -1
ciclo= 0
x1=333
x2=1130
x3=352
x4=1077
y1=22
y2=77
y3=665
y4=540

def trans_afin (event, x, y, flags, param):
    global img1, img2, img_cpy, ciclo, x1, y1, x2, y2, x3, y3, x4, y4
    if (event == cv2.EVENT_LBUTTONDOWN) & (ciclo == 1):
        img1=img_cpy
        pts1= np.float32 ([[x1, y1], [x2, y2], [x3, y3], [x4, y4]])
        pts2= np.float32 ([[0, 0], [1170, 0], [0, 750], [1170, 750]])
        M= cv2.getPerspectiveTransform(pts1, pts2)                              # El orden de los puntos es importante
        img2= cv2.warpPerspective (img1, M, (1170,750))                         # La imagen 2 es la que sufre la transformación
        cv2.imshow('image2', img2)
        ciclo= ciclo+1
        print ('Se rectifica sobre una ventana de dimensiones 1170 px x 750 px manteniendo la misma relación de aspecto 1.56. Haga click para continuar...\n')
        print ('Selecciona 2 puntos sobre la ventana "image2" para calcular la distancia que los separa.\n')
    if (event == cv2.EVENT_LBUTTONDOWN) & (ciclo == 0):
        cv2.circle(img1, (x1, y1), 5, red, -1)
        cv2.circle(img1, (x2, y2), 5, red, -1)
        cv2.circle(img1, (x3, y3), 5, red, -1)
        cv2.circle(img1, (x4, y4), 5, red, -1)
        ciclo= ciclo+1
        print ('Se utilizan estos 4 puntos para delimitar el plano definido por la tabla de dimensiones 39.2 cm x 25.2 cm. Relación de aspecto 1.56.\n')

def medicion (event, x, y, flags, param):
    global img2, ciclo, x1, y1, x2, y2
    if (event == cv2.EVENT_LBUTTONDOWN) & (ciclo == 3):
        cv2.circle(img2, (x, y), 5, red, -1)
        cv2.imshow('image2', img2)   
        x2= x
        y2= y
        c1= abs(x1-x2)
        c2= abs(y1-y2)
        h=math.sqrt((c1*c1)+(c2*c2))
        dist= (h*39.2)/1170
        print ('Distancia entre los puntos {:0.2f} cm.\n' .format(dist))
        cv2.imwrite('Recorte.png', img2)
        print ('Imagen guardada.\n')
        ciclo= ciclo+1
    if (event == cv2.EVENT_LBUTTONDOWN) & (ciclo == 2):
        cv2.circle(img2, (x, y), 5, red, -1)
        cv2.imshow('image2', img2)   
        x1= x
        y1= y
        ciclo= ciclo+1


img1= cv2.imread('Muestra.jpeg')                                                # Imagen/matriz de zeros (negra) de 512x512 con 3 canales y cada elemento del tipo que quiera (int, float, etc).
img_cpy= cv2.imread('Muestra.jpeg')
cv2.namedWindow('image1')                                                       # Crea una ventana de nombre: image1.
cv2.namedWindow('image2')                                                       # Crea una ventana de nombre: image2.

while (1):
    cv2.imshow('image1', img1)                                                  # Muestra img1 en la ventana image creada anteriormente.
    k= cv2.waitKey(1) & 0xFF
    if k == ord ('a'): 
        print ('\nHaga click en la ventana "image1" para continuar...\n')
        cv2.setMouseCallback('image1', trans_afin)
        cv2.setMouseCallback('image2', medicion)
        enable= 2
    elif k == ord ('g'):                                                        # Guarda el recorte al apretar la tecla g.
        cv2.imwrite('Recorte.png', img2)
        print ('Imagen guardada.')
    elif k == ord ('r'):                                                        # Restaura la imagen al apretar la tecla r.
        img1= cv2.imread('Muestra.jpeg')  
        ciclo= 0
        x1=333
        x2=1130
        y1=22
        y2=77
        print ('Imagen restaurada.')
    elif k == ord ('q'):                                                        # Finaliza el programa al apretar la tecla q.
        print ('Programa finalizado.')
        break
cv2.destroyAllWindows()
