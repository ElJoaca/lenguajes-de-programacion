import cv2
import numpy as np
import glob

class calibracion ():
    def __init__(self):
        self.tablero= (7,5)                                                                                                 # Esquina horizontal y vertical del tablero de ajedrez respecticamente.
        self.tam_frame= (1920, 1080)                                                                                        # Resolución de la cámara.

        self.criterio= (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)                                      # Criterio con el que definimos las esquinas.

        self.puntos_obj= np.zeros((self.tablero[0] * self.tablero[1], 3), np.float32)                                       # Preparamos los puntos del tablero.
        self.puntos_obj[:,:2]= np.mgrid[0: self.tablero[0], 0: self.tablero[1]].T.reshape(-1,2)

        self.puntos_3d= []                                                                                                  # Preparamos las listas para almacenar los puntos del mundo real y de la imagen.
        self.puntos_img= []

    def calibracion_cam(self):
        fotos= glob.glob('*.jpg')                                                                                               # Leemos las imagenes .jpg de calibración de cámara
        for foto in fotos:
            print (foto)
            img= cv2.imread (foto)
            gray= cv2.cvtColor (img, cv2.COLOR_BGR2GRAY)                                                                        # Pasamos las imagenes en escala de grises.

            ret, esquinas= cv2.findChessboardCorners (gray, self.tablero, None)                                                 # Detectamos las esquinas.

            if ret == True:
                self.puntos_3d.append (self.puntos_obj)
                esquinas2= cv2.cornerSubPix (gray, esquinas, (11,11), (-1,-1), self.criterio)
                self.puntos_img.append(esquinas)
                #cv2.drawChessboardCorners (img, self.tablero, esquinas2, ret)
                #cv2.imshow ("img", img)

        ret, cameraMatrix, dist, rvecs, tvecs= cv2.calibrateCamera (self.puntos_3d, self.puntos_img, self.tam_frame, None, None)    #Calibracion de la camara

        return cameraMatrix, dist