import cv2 as cv
import numpy as np
from Calibracion import *

parameters= cv.aruco.DetectorParameters_create ()                           # Inicializamos los prametros del detector de aruco.  

dictionary= cv.aruco.Dictionary_get(cv.aruco.DICT_4X4_250)                  # Cargamos el diccionario de nuestro aruco.

cap= cv.VideoCapture (0)                                                    # Lectura de la cámara.
cap.set (3, 1280)                                                           # Definimos un ancho y un alto.
cap.set (4, 720)
cont= 0

calibracion= calibracion ()                                                 # Calibracion
matrix, dist= calibracion.calibracion_cam ()
print ("Matriz de la cámara: ", matrix)
print ("Coeficiente de Distorsion: ", dist)

while True:
    ret, frame= cap.read()
    gray= cv.cvtColor (frame, cv.COLOR_BGR2GRAY)
    corners, ids, rejected= cv.aruco.detectMarkers (gray, dictionary, parameters=parameters, cameraMatrix= matrix, distCoeff= dist)

    try:
        if np.all(ids is not None):
            for i in range (0, len(ids)):
                rvec, tvec, markerPoints= cv.aruco.estimatePoseSingleMarkers (esquina[i], 0.02, matrix, dist)
                (rvec - tvec).any ()
                cv.aruco.drawDetectedMarkers (frame, corners)               # Dibuja el contorno alrededor del aruco.
                cv.aruco.drawAxis (frame, matrix, dist, rvec, tvec, 0.01)   # Dibujamos los ejes
                c_x= (corners[i][0][0][0] + corners[i][0][1][0] + corners [i][0][2][0] + corners[i][0][3][0]) / 4
                c_y= (corners[i][0][0][1] + corners[i][0][1][1] + corners [i][0][2][1] + corners[i][0][3][1]) / 4
                cv.putText (frame, "id" + str(ids[i]), (int(c_x), int(c_y)), cv.FONT_HERSHEY_SIMPLEX, 0.5, (50, 225, 250), 2)

                c1= (corners[0][0][0][0], corners[0][0][0][1])
                c2= (corners[0][0][1][0], corners[0][0][1][1])
                c3= (corners[0][0][2][0], corners[0][0][2][1])
                c4= (corners[0][0][3][0], corners[0][0][3][1])
                v1, v2= c1[0], c1[1]
                v3, v4= c2[0], c2[1]
                v5, v6= c3[0], c3[1]
                v7, v8= c4[0], c4[1]



                cv.line(frame, (int(v1), int(v2)), (int(v3), int(v4)), (255, 255, 0), 3)
                cv.line(frame, (int(v5), int(v6)), (int(v7), int(v8)), (255, 255, 0), 3)
                cv.line(frame, (int(v1), int(v2)), (int(v7), int(v8)), (255, 255, 0), 3)
                cv.line(frame, (int(v3), int(v4)), (int(v5), int(v6)), (255, 255, 0), 3)

                cv.line(frame, (int(v1), int(v2 - 200)), (int(v3), int(v4 - 200)), (255, 255, 0), 3)
                cv.line(frame, (int(v5), int(v6 - 200)), (int(v7), int(v8 - 200)), (255, 255, 0), 3)
                cv.line(frame, (int(v1), int(v2 - 200)), (int(v7), int(v8 - 200)), (255, 255, 0), 3)
                cv.line(frame, (int(v3), int(v4 - 200)), (int(v5), int(v6 - 200)), (255, 255, 0), 3)

                cv.line(frame, (int(v1), int(v2 - 200)), (int(v1), int(v2)), (255, 255, 0), 3)
                cv.line(frame, (int(v3), int(v4 - 200)), (int(v3), int(v4)), (255, 255, 0), 3)
                cv.line(frame, (int(v5), int(v6 - 200)), (int(v5), int(v6)), (255, 255, 0), 3)
                cv.line(frame, (int(v7), int(v8 - 200)), (int(v7), int(v8)), (255, 255, 0), 3)





                cv.line(frame, (int(v1), int(v2)), (int(v3), int(v4)), (255, 0, 255), 3)
                cv.line(frame, (int(v5), int(v6)), (int(v7), int(v8)), (255, 0, 255), 3)
                cv.line(frame, (int(v1), int(v2)), (int(v7), int(v8)), (255, 0, 255), 3)
                cv.line(frame, (int(v3), int(v4)), (int(v5), int(v6)), (255, 0, 255), 3)

                cex1, cey1= (v1 + v5) // 2, (v2 + v6) // 2
                cex2, cey2= (v3 + v7) // 2, (v4 + v8) // 2
                cv.line (fram, (int(v1), int(v2)), (int(cex1), int(cey1 - 200)), (255, 0, 255), 3)
                cv.line (fram, (int(v5), int(v6)), (int(cex1), int(cey1 - 200)), (255, 0, 255), 3)
                cv.line (fram, (int(v3), int(v4)), (int(cex1), int(cey2 - 200)), (255, 0, 255), 3)
                cv.line (fram, (int(v7), int(v8)), (int(cex1), int(cey2 - 200)), (255, 0, 255), 3)


    except:
        if ids is None or len (ids) == 0:
            print ("Fallo en la deteccion del marcador")

    cv.imshow ('Realidad Virtual', frame)

    k= cv.waitKey(1)

    if k == 97:
        print ("Imagen guardada")
        cv.imwrite("cali{}.png".format(cont), frame)
        cont = cont + 1
    if k == 27:
        break

cap.release()
cv.destroyAllWindows()