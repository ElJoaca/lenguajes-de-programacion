import cv2 as cv
import numpy as np 

copy= 0
cont= 0

dictionary= cv.aruco.Dictionary_get (cv.aruco.DICT_4X4_250)                                             # Traemos el diccionario con el que generamos el marcador.

parameters= cv.aruco.DetectorParameters_create()                                                        # Creamos los parámetros del detector.

cap= cv.VideoCapture (0)                                                                                # Abrimos la cámara.
#cap.set (3, 1920)
#cap.set (4, 1080)

while True:
    ret, frame= cap.read()                                                                              # Leo una imagen.
    gray= cv.cvtColor (frame, cv.COLOR_BGR2GRAY)

    esquinas, ids, rejected= cv.aruco.detectMarkers (gray, dictionary, parameters= parameters)          # Detectamos y ubica las esquinas de los marcadores. Me devuelve la esquina, el id y algunos que los encontró pero no los pudo detectar bien.

    if np.all (ids != None):
        aruco= cv.aruco.drawDetectedMarkers (frame, esquinas)                                           # Dibujo los marcadores detectados.

        c1= (esquinas[0][0][0][0], esquinas[0][0][0][1])
        c2= (esquinas[0][0][1][0], esquinas[0][0][1][1])
        c3= (esquinas[0][0][2][0], esquinas[0][0][2][1])
        c4= (esquinas[0][0][3][0], esquinas[0][0][3][1])

        copy= frame

        imagen= cv.imread("imagen1.jpeg")

        tamaño= imagen.shape

        puntos_aruco= np.array ([c1, c2, c3, c4])

        puntos_imagen= np.array ([
            [0, 0],
            [tamaño[1] -1, 0],
            [tamaño[1] -1, tamaño[0] - 1],
            [0, tamaño[0] - 1]
        ], dtype= float)

        h, estado= cv.findHomography (puntos_imagen, puntos_aruco)

        perspectiva= cv.warpPerspective (imagen, h, (copy.shape[1], copy.shape[0]))
        cv.fillConvexPoly (copy, puntos_aruco.astype (int), 0, 16)
        copy= copy + perspectiva
        cv.imshow ("Realidad Virtual", copy)
    
    else:
        cv.imshow ('Realidad Virtual', frame)
    
    k= cv.waitKey (1)
    if k == ord ('g'):
        print ("Imagen guardada")
        cv.imwrite("aruco{}.png".format(cont), copy)
        cont += 1
    if k == 27:                                                                                                 # Si presiono escape finalizo el programa.
        break

cap.release()
cv.destroyAllWindows ()